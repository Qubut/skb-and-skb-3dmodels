import {
  Directive,
  ElementRef,
  HostBinding,
  HostListener,
  Input,
  Renderer2,
} from '@angular/core';

@Directive({
  selector: '[appHover]',
})
export class HoverDirective {
  @Input() public appHover: string[] = [];
  @HostListener('mouseout') onMouseLeave() {
    if(this.appHover.length%2==0){
    this._renderer.removeClass(this._el.nativeElement, this.appHover[1]);
    this._renderer.addClass(this._el.nativeElement, this.appHover[0]);
    this._renderer.removeClass(this._el.nativeElement, this.appHover[3]);
    this._renderer.addClass(this._el.nativeElement, this.appHover[2]);
    }
  }
  @HostListener('mouseover') onMouseEnter() {
    this._renderer.removeClass(this._el.nativeElement, this.appHover[0]);
    this._renderer.addClass(this._el.nativeElement, this.appHover[1]);
     this._renderer.removeClass(this._el.nativeElement, this.appHover[2]);
    this._renderer.addClass(this._el.nativeElement, this.appHover[3]);

  }
  constructor(private _renderer: Renderer2, private _el: ElementRef) {}
}
