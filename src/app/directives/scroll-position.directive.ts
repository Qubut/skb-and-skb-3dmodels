import {
  Directive,
  HostListener,
  Inject,
  Input,
  OnDestroy,
  OnInit,
  PLATFORM_ID,
} from '@angular/core';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { setScrollPosition } from '@skb/stores/scroll-position/scroll-position.action';
import { ScrollPositionState } from '@skb/interfaces/scroll-position-state';
import { isPlatformBrowser } from '@angular/common';

@Directive({
  selector: '[appScrollPosition]',
})
export class ScrollPositionDirective implements OnInit, OnDestroy {
  @Input() scrollPosition: string = '';

  private scrollPositionSubscription = new Subscription();

  constructor(
    private store: Store<{ scrollPosition: ScrollPositionState }>,
    @Inject(PLATFORM_ID) private platformId: Object
  ) {}

  ngOnInit() {
    if (isPlatformBrowser(this.platformId)){
    this.scrollPositionSubscription = this.store
      .select((state) => state.scrollPosition[this.scrollPosition])
      .subscribe((scrollPosition) => {
        if (scrollPosition) {
          window.scrollTo(0, scrollPosition);
        }
      });
      }  }

  ngOnDestroy() {
    this.scrollPositionSubscription.unsubscribe();
  }

  @HostListener('window:scroll')
  onScroll() {
    if (isPlatformBrowser(this.platformId)){
    const currentPosition = window.pageYOffset;
    this.store.dispatch(
      setScrollPosition({
        component: this.scrollPosition,
        position: currentPosition,
      })
    );
  }
}}
