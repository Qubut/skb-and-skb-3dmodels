import { TestBed } from '@angular/core/testing';

import { FachvortraegeService } from './fachvortraege.service';

describe('FachvortraegeService', () => {
  let service: FachvortraegeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FachvortraegeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
