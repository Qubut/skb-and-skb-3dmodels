import { TestBed } from '@angular/core/testing';

import { ModelSpecificationService } from './model-specification.service';

describe('ModelSpecificationService', () => {
  let service: ModelSpecificationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ModelSpecificationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
