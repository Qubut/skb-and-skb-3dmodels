import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class VideoLinkService {
  // Define regular expressions for each video platform
  private _regExpYoutube =
    /^.*(youtu.be\/|v\/|u\/\w\/|shorts\/|embed\/|watch\?v=|\&v=)([^#\&\?]{11,11}).*/;
  private _regExpVimeo =
    /^.*vimeo\.\w+\/(?:channels\/(?:\w+\/)?|groups\/(?:[^\/]*)\/videos\/|video\/|)(\d+)(?:|\/?|\&h\=)(\d+.+)/;

  private _regExpFacebook = /(?:videos|story_fbid)(?:\/|=)(\d+)(?:\/|&)?/;

  constructor() {}

  /**
   * Extracts video URLs and thumbnail images from an array of video URLs
   * @param videoUrls - An array of video URLs
   * @returns An object with two properties: videoUrls and videoUrlImages
   */
  getVideoUrls(videoUrls: string[]) {
    // Initialize an empty array to hold thumbnail images
    const videoUrlImages: string[] = [];

    // Check if the videoUrls array is not empty
    if (videoUrls.length) {
      // Map over the videoUrls array and extract video URLs and thumbnail images
      const mappedUrls = videoUrls.map((u, i) => {
        // Check if the video is from YouTube
        let match = u.match(this._regExpYoutube);
        if (match && match.length >= 2) {
          videoUrlImages[i] = `/vi/${match[2]}/0.jpg`;
          return `https://youtube.com/watch?v=${match[2]}`;
        }

        // Check if the video is from Vimeo
        let matchVimeo = u.match(this._regExpVimeo);
        if (matchVimeo && matchVimeo.length >= 2) {
          // Fetch the video metadata to extract the thumbnail image
          fetch(`https://vimeo.com/api/oembed.json?url=${u}`)
            .then((response) => {
              response
                .json()
                .then((data) => {
                  videoUrlImages[i] = data.thumbnail_url;
                })
                .catch(() => {
                  // Set a default thumbnail image if the metadata cannot be fetched
                  videoUrlImages[i] = '/assets/svg/video-player.svg';
                });
            })
            .catch((e) => {
              console.dir(e);
            });
          return matchVimeo[1]
            ? `https://player.vimeo.com/video/${matchVimeo[1]}?h=${matchVimeo[2]}`
            : `https://player.vimeo.com/video/${matchVimeo[2]}`;
        }

        // Check if the video is from Facebook
        let matchFacebook = u.match(this._regExpFacebook);
        if (matchFacebook && matchFacebook.length) {
          videoUrlImages[i] = '/assets/svg/video-player.svg';
          return `https://facebook.com/plugins/video.php?href=${u}`;
        }

        // If the video platform cannot be identified, set the thumbnail image to an empty string
        videoUrlImages[i] = '';
        return '';
      });

      // Return an object with the mapped video URLs and thumbnail images
      return { videoUrls: mappedUrls, videoUrlImages };
    }

    // If the videoUrls array is empty, return an empty object
    return { videoUrls: [], videoUrlImages: [] };
  }
}
