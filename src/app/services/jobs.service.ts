import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class JobsService {

  private readonly _jobs = new BehaviorSubject<string[]>([]);
  readonly jobs$: Observable<string[]> = this._jobs.asObservable();

  addJob(job: string) {
    this._jobs.next([...this._jobs.value, job]);
  }

  removeJob(job: string) {
    this._jobs.next(this._jobs.value.filter((j) => j !== job));
  }

  reset() {
    this._jobs.next([]);
  }
}
