import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import {
  catchError,
  from,
  lastValueFrom,
  map,
  mergeMap,
  of,
  switchMap,
} from 'rxjs';

interface FileData {
  file_name: string;
  file: string;
}
@Injectable({
  providedIn: 'root',
})
export class UploadFilesService {
  /**
   * Service for uploading files to Strapi.
   * @param http - The HTTP client service.
   */
  constructor(private http: HttpClient) {}

  /**
   * Uploads a single media file to Strapi using the Strapi Media Library API.
   *
   * @param file A dictionary containing information about the file to be uploaded.
   *             The dictionary should have two keys: 'fileName' and 'file'.
   *             'fileName' is a string representing the name of the file.
   *             'file' is a string representing the URL of the file.
   * @return The ID of the uploaded file, if the upload was successful. Otherwise, returns null.
   */
  public async uploadSingleMedia(file: FileData): Promise<number | null> {
    // Extract the file name and link from the file object.
    if (!file.file || !file.file_name) return null;
    const fileName = file.file_name.replace(/[\?]/g, '');
    const fileLink = file.file;

    // If the file link is empty, return null immediately.
    if (fileLink === '') {
      return null;
    }

    // Determine the file type based on the file name extension.
    const fileType = fileName.endsWith('.pdf') ? 'application/pdf' : 'image';

    // Set the HTTP headers and options for the file upload request.
    const headers = new HttpHeaders({ 'Content-Type': fileType });
    const options = { headers, reportProgress: true };

    // Create the observable that represents the file upload process. It first downloads the file from the given link and then uploads it to the server.
    return lastValueFrom(
      this.http.get('https:' + fileLink + '?', { responseType: 'blob' }).pipe(
        switchMap((fileBlob) => {
          let result;
          const fd = new FormData();
          fd.append('files', fileBlob);
          return from(
            fetch(`api/upload`, {
              method: 'post',
              body: fd,
            })
          );
        }),
        mergeMap(async (res) => {
          const response = await res.json();
          // Extract the file ID from the server response, or set it to null if the response is not an array or does not contain any IDs.
          const id = Array.isArray(response) ? response[0].id : null;
          // If the file was successfully uploaded, log a message with its name and ID.
          if (id) {
            console.info(`Uploaded file '${fileName}' with ID '${id}'.`);
          }
          return id;
        }),
        catchError((error) => {
          console.log(error);
          // If there was an error during the file upload process, log an error message and return null.
          console.error(`Error uploading file '${fileName}': ${error.message}`);
          return of(null);
        })
      )
    );
  }

  /**
   * Uploads multiple media files to Strapi using the Strapi Media Library API.
   *
   * @param files An array where each element is a dictionary containing information about a file to be uploaded.
   *              Each dictionary should have two keys: 'fileName' and 'file'.
   *              'fileName' is a string representing the name of the file.
   *              'file' is a string representing the URL of the file.
   * @return An array of IDs corresponding to the uploaded files, in the same order as the input array.
   */
  public async uploadMultipleMedia(files: FileData[]): Promise<number[]> {
    if (!files.length) return [];
    // Create an array of promises, one for each file in the input array.
    // Each promise represents the process of uploading one file to the server.
    const promises: Promise<number | null>[] = files.map((file) =>
      this.uploadSingleMedia(file)
    );

    try {
      // Execute all the promises in parallel and wait for all of them to complete.
      const ids = await Promise.all(promises);
      // Filter out any null values from the array of file IDs and return the filtered array as an array of strings.
      const idsWithoutNulls = ids.filter((id) => id !== null);
      return idsWithoutNulls as number[];
    } catch (error) {
      console.log(error);
      // If there was an error during the file upload process, log an error message and return an empty array.
      console.error(`Error uploading files: ${error}`);
      return [];
    }
  }
}
