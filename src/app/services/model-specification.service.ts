import { Inject, Injectable } from '@angular/core';
type Scale = [[number, number], [number, number]];
@Injectable({
  providedIn: 'root',
})
export class ModelSpecificationService {
  private logos: Bild[] = [];
  public logosUrls: string[] = [];
  public Bilder: Bild[] = [];
  private isB: boolean = false;
  public scaling: [[number, number], [number, number]] = [
    [1, 1],
    [1, 1],
  ];
  private aspectRatio = {
    a: 8 / 3.5,
    b: 8 / 1.6,
  };
  public thekeplatte = '';
  constructor(
    @Inject('onlinemedien')
    data: {
      o: OnlineMedien;
      firmenlogo: Bild;
      logo: Bild;
      messestand: string;
    }
  ) {
    const o = data.o;
    this.logos = [data.logo, data.firmenlogo].filter((e) => e);
    this.Bilder = o.bilder && o.bilder.length ? o.bilder :this.fillBilder();

    if (this.logos.length > 1)
      this.logosUrls = [this.logos[0].url, this.logos[1].url];
    else if (this.logos.length == 1) {
      this.logos.push(this.logos[0]);
      this.logosUrls = [this.logos[0].url, this.logos[0].url];
    } else {
      this.logosUrls = ['', '', ''];
    }
    this.logosUrls.unshift(this.logosUrls[0]);
    this.logosUrls.unshift(
      this.Bilder[0].url.length ? this.Bilder[0].url : this.logosUrls[0]
    );
    this.isB = data.messestand == 'Messestand B';
    this.thekeplatte = this.isB
      ? `./assets/models/floor2.jpg`
      : `./assets/models/floor2.jpg`;
    this.logos.forEach((l, i) => {
      let ih = l.height!;
      let iw = l.width!;
      let aspectRatio = iw / ih;
      let fw = 0;
      let fh = 0;
      if (
        (this.isB && aspectRatio > this.aspectRatio.b) ||
        (!this.isB && aspectRatio > this.aspectRatio.a)
      ) {
        let width_ratio = 8 / iw;
        let height_ratio = this.isB ? 1.6 / ih : 3.5 / ih;

        if (width_ratio > height_ratio) {
          fw = iw * width_ratio;
          fh = (ih * fw) / iw;
          if ((this.isB && fh > 1.6) || fh > 3.5) fh = this.isB ? 1.6 : 3.5;
        } else {
          fh = ih * height_ratio;
          fw = (iw * fh) / ih;
          fw = fw > 8 ? 8 : fw;
        }
        this.scaling[i] = this.isB ? [fh, fw] : [fw, fh];
      } else {
        if (this.isB) {
          fh = 1.6;
          fw = 1.6 * aspectRatio;
          fw = fw < 4 ? 4 : fw;
          this.scaling[i] = [fh, fw];
        } else {
          fh = 3.5;
          fw = 3.5 * aspectRatio;
          fw = fw < 4 ? 4 : fw;
          this.scaling[i] = [fw, fh];
        }
      }
    });
  }
  fillBilder():Bild[]{
    if(this.logos.length)
    return [{url:this.logos[0].url}]
  else return[{url:''}]
  }
}
