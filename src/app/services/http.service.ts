import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})

export class HttpService {

  constructor(private httpClient: HttpClient) { }

  public get(endpoint:string, params?:HttpParams): Observable<any> {
    return this.httpClient.get(endpoint, {params});
  }

  public post(endpoint:string, body:any): Observable<any> {
    return this.httpClient.post(endpoint, body);
  }

  public put(endpoint:string, body:any): Observable<any> {
    return this.httpClient.put(endpoint, body);
  }

  public delete(endpoint:string, params?:HttpParams): Observable<any> {
    return this.httpClient.delete(endpoint, {params});
  }

}