import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DefaultDataService, HttpUrlGenerator } from '@ngrx/data';
import { map, Observable } from 'rxjs';
import { shareReplay } from 'rxjs/operators';



@Injectable()
export class HomepageDataService extends DefaultDataService<Homepage> {
  private homepageCache$: Observable<Homepage[]> | undefined;

  constructor(
    http: HttpClient,
    httpUrlGenerator: HttpUrlGenerator
  ) {
    super('Homepage', http, httpUrlGenerator);
  }

  override getAll(): Observable<Homepage[]> {
    if (!this.homepageCache$) {
      const params = {
        _sort: 'id',
        _order: 'asc',
        '[populate][begruessungsmedia][populate]': '*',
        '[populate][alumniStorie][populate]': '*',
        '[populate][infokarten][populate]': '*',
        '[populate][skblogo][populate][url][populate]': '*',
        '[populate][aussteller][id][populate]': '*',
      };
      const url = `/api/homepage`;

      this.homepageCache$ = this.http.get<{ data: Homepage[] }>(url, { params })
        .pipe(
          map(response => response.data),
          shareReplay(1)
        );
    }

    return this.homepageCache$;
  }
}
