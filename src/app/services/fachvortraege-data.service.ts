import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DefaultDataService, HttpUrlGenerator } from '@ngrx/data';
import { from, map, Observable } from 'rxjs';
import { groupBy, mergeMap, shareReplay, toArray } from 'rxjs/operators';


@Injectable()
export class FachvortraegeDataService extends DefaultDataService<Fachvortrag> {
  private fachvortraegeCache$: Observable<Fachvortrag[]> | undefined;

  constructor(http: HttpClient, httpUrlGenerator: HttpUrlGenerator) {
    super('Fachvortraege', http, httpUrlGenerator);
  }

  override getAll(): Observable<Fachvortrag[]> {
    if (!this.fachvortraegeCache$) {
      const params = new HttpParams()
        .set('[populate][aussteller][populate][logo][populate]', '*')
        .set('[populate][aussteller][populate][firmenlogo][populate]', '*');
      const url = `api/fachvortrags`;

      this.fachvortraegeCache$ = this.http
        .get<Fachvortraege>(url, { params })
        .pipe(
          map((f) => f.data),
          mergeMap((data) => from(data)),
          groupBy((element) => element.datum),
          mergeMap((group$) =>
            group$.pipe(
              toArray(),
              map((elements) =>
                elements.sort((a, b) => a.start_time.localeCompare(b.start_time))
              ),
              map((elements) => ({ [group$.key]: elements }))
            )
          ),
          toArray(),
          map((groups) => Object.assign({}, ...groups)),shareReplay(1)
        );
    }
    return this.fachvortraegeCache$;
  }
}
