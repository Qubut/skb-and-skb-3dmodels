import { TestBed } from '@angular/core/testing';

import { VideoLinkService } from './video-link.service';

describe('VideoLinkService', () => {
  let service: VideoLinkService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VideoLinkService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
