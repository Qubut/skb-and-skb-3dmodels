import { Injectable } from '@angular/core';



/**
 * A service that deletes old files from the server.
 */
@Injectable({
  providedIn: 'root',
})
export class DeleteOldFilesService {
  /**
   * Constructs a new instance of the DeleteOldFilesService.
   * @param data An array of IDs of the files to be deleted.
   */
  constructor() {}

  /**
   * Deletes the old files from the server.
   * @returns void
   */
  delete = (files: number[]): Promise<Object>[] => {
    return files.map((f) =>
      fetch(`api/upload/files/${f}`, {
        method: 'DELETE',
      })
    );
  };
}
