import { TestBed } from '@angular/core/testing';

import { FakultätenService } from './fakultäten.service';

describe('FakultätenService', () => {
  let service: FakultätenService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FakultätenService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
