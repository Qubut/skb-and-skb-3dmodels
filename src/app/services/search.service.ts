import { Injectable } from '@angular/core';
import { MeiliSearch } from 'meilisearch';
import { environment } from '@env/environment';

@Injectable({
  providedIn: 'root',
})
export class SearchService {
  private readonly meilieKey = environment.meilisearchKey;
  private searchClient = new MeiliSearch({
    host: environment.endpoints.meilisearch,
    headers: {
      Authorization: `Bearer ${this.meilieKey}`,
      'Content-Type': 'application/json',
    },
  });

  constructor() {}

  async getNbOfEntries(indexName: string, query:string, filter:string) {
       return (<any>(await this.searchClient
      .index(indexName)
      .search(query, {filter})))['estimatedTotalHits'];
  }

  async search(
    query: string,
    limit: number,
    offset: number,
    filter: string,
    indexName:string
  ) {
    const sort = indexName=='aussteller'? ['firma:asc']:[]
    const searchParams = { limit, filter, offset,sort};
    const searchResults = await this.searchClient
      .index(indexName)
      .search(query, searchParams);
    return searchResults.hits;
  }
}
