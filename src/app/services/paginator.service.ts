import { Injectable, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable, BehaviorSubject, lastValueFrom, Subscription } from 'rxjs';
import { PaginatorState } from '@skb/interfaces/paginator-state';

const PAGINATOR_STATE_KEY = 'paginator_state';

@Injectable({
  providedIn: 'root',
})
export class PaginatorService implements OnDestroy {
  private _paginator$ = new BehaviorSubject<PaginatorState>({
    currentPage: 0,
    pageSize: 10,
    totalLength: 0,
    pageSizeOptions: [10, 25, 50, 100],
  });
  totalLength: number = 0;
  pageSizeOptions: number[] = [];
  loadStateSubscribtion = new Subscription();
  constructor(private store: Store<{ paginator: PaginatorState }>) {
    this.loadStateSubscribtion = this.loadState()!.subscribe((data) => {
      this._paginator$.next(data);
    });
  }
  ngOnDestroy(): void {
    this.loadStateSubscribtion.unsubscribe();
  }

  get paginator$(): Observable<PaginatorState> {
    return this._paginator$.asObservable();
  }

  private saveState(state: PaginatorState) {
    localStorage.setItem(PAGINATOR_STATE_KEY, JSON.stringify(state));
  }

  private loadState(): Observable<PaginatorState> | null {
    try {
      const state = this.store.select((k) => k.paginator);
      return state;
    } catch (error) {
      console.error(error);
      return null;
    }
  }
}
