import { TestBed } from '@angular/core/testing';

import { FetchProfairsDataService } from './fetch-profairs-data.service';

describe('FetchProfairsDataService', () => {
  let service: FetchProfairsDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FetchProfairsDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
