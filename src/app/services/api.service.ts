import { Injectable } from '@angular/core';
import { lastValueFrom, Observable } from 'rxjs';
import { distinctUntilChanged, map, shareReplay, tap } from 'rxjs/operators';
import { HttpClient, HttpParams } from '@angular/common/http';


@Injectable({
  providedIn: 'root',
})
export class ApiService {
  commonParams = new HttpParams()
    .set('populate', '*')
    .set('[pagination][limit]', 2000);
  constructor(private _httpClient: HttpClient) {}
  getStellenausschreibungen(): Observable<Stellenausschreibung[]> {
    const params = new HttpParams({
      fromObject: {
        '[populate][aussteller][populate][logo][populate]': '*',
        '[populate][aussteller][populate][firmenlogo][populate]': '*',
        '[populate][studiengangs][populate]': '*',
        '[populate][file][populate]': '*',
      },
    });
    return this._httpClient
      .get<{ data: Stellenausschreibung[] }>(
        `/api/stellenausschreibungen`,
        { params: this.commonParams, observe: 'body' }
      )
      .pipe(map((response) => response.data));
  }
  getFirmenOfMessehalle(): Observable<Aussteller[]> {
    return this._httpClient
      .get<{ data: Aussteller[] }>(
        `/api/ausstellers`,
        { params: this.commonParams, observe: 'body' }
      )
      .pipe(map((response) => response.data));
  }

  getAussteller(slug: string): Observable<Aussteller> {
    const params = new HttpParams({
      fromObject: {
        'populate[onlinemedien][populate]': '*',
        'populate[ansprechpartner][populate]': '*',
        'populate[stellenausschreibungen][populate]': '*',
        'populate[logo][populate]': '*',
        'populate[firmenlogo][populate]': '*',
        'populate[messestand][populate]': '*',
      },
    });
    return this._httpClient
      .get<{ data: Aussteller }>(
        `/api/slugify/slugs/aussteller/${slug}`,
        { params, observe: 'body' }
      )
      .pipe(
        map((r) => {
          if(!r.data.onlinemedien)
            return r.data
          const videoUrls = r.data.onlinemedien.videoUrls;
          const socialmedien = r.data.onlinemedien.socialmedien;
          const socialmedia: any = {};
          socialmedien.forEach((sm) => {
            if (/facebook/.test(sm)) {
              socialmedia.facebook = sm;
            } else if (/instagram/.test(sm)) {
              socialmedia.instagram = sm;
            } else if (/linkedin/.test(sm)) {
              socialmedia.linkedin = sm;
            } else if (/xing/.test(sm)) {
              socialmedia.xing = sm;
            } else if (/youtube/.test(sm)) {
              socialmedia.youtube = sm;
            } else if (/twitter/.test(sm)) {
              socialmedia.twitter = sm;
            }
          });

          const addHttps = (url: string): string =>
            url && !/^https?:\/\//i.test(url) ? `https://${url}` : url;

          r.data.onlinemedien.socialmedia = socialmedia;
          r.data.onlinemedien.videoUrls =
            <any>videoUrls instanceof String
              ? videoUrls
              : Array.isArray(videoUrls)
              ? videoUrls
              : JSON.parse(<any>videoUrls);
          return r.data;
        })
      );
  }

  getMessepartners(gewinnspiel: boolean = false): Observable<Messepartners> {
    const gewinnspiel_params = new HttpParams({
      fromObject: {
        'populate[ausstellers][populate][logo][populate]': '*',
        'populate[ausstellers][populate][firmenlogo][populate]': '*',
        'populate[premium_ausstellers][populate]': '*',
      },
    });
    const params = new HttpParams({
      fromObject: {
        'populate[ausstellers][populate][logo][populate]': '*',
        'populate[ausstellers][populate][firmenlogo][populate]': '*',
        'populate[premium_aussteller][populate]': '*',
      },
    });
    if (gewinnspiel)
      return this._httpClient
        .get<{ data: Messepartners }>(
          `/api/gewinnspielsmessepartner`,
          { params: gewinnspiel_params, observe: 'body' }
        )
        .pipe(map((response) => response.data));

    return this._httpClient
      .get<{ data: Messepartners }>(
        `/api/messepartner`,
        { params, observe: 'body' }
      )
      .pipe(map((response) => response.data));
  }

  getFakultaeten(): Observable<Fakultaet[]> {
    return this._httpClient
      .get<{ data: Fakultaet[] }>(
        `/api/fakultaets`,
        { params: this.commonParams, observe: 'body' }
      )
      .pipe(
        map((r) => r.data),
        tap((arr) =>
          arr.forEach((f) => f.studiengangs.forEach((s) => (s.checked = false)))
        ),
        distinctUntilChanged(),
        shareReplay({ bufferSize: 1, refCount: true })
      );
  }
  getJobarten(): Observable<Jobart[]> {
    return this._httpClient
      .get<{ data: Jobart[] }>(`/api/jobarts`)
      .pipe(
        map((r) => r.data),
        tap((arr) => arr.forEach((j) => (j.checked = false)))
      );
  }
  getNavigationsleiste(): Observable<Item[]> {
    return this._httpClient
      .get<Navigationsleiste>(
        `/api/menus/1?nested&populate=*`,
        {
          observe: 'body',
        }
      )
      .pipe(
        map((r) => r.data.items),
        shareReplay(1)
      );
  }
  getFachvortraege(): Observable<Fachvortraege> {
    const params = new HttpParams({
      fromObject: {
        '[populate][aussteller][populate][logo][populate]': '*',
        '[populate][aussteller][populate][firmenlogo][populate]': '*',
      },
    });
    return this._httpClient.get<Fachvortraege>(
      `/api/fachvortrags`,
      { params, observe: 'body' }
    );
  }
  getHomepage(): Observable<Homepage> {
    const params = new HttpParams({
      fromObject: {
        '[populate][begruessungsmedia][populate]': '*',
        '[populate][alumniStorie][populate]': '*',
        '[populate][infokarten][populate]': '*',
        '[populate][skblogo][populate][url][populate]': '*',
        '[populate][aussteller][id][populate]': '*',
      },
    });
    return this._httpClient
      .get<{ data: Homepage }>(`/api/homepage`, {
        params,
        observe: 'body',
      })
      .pipe(map((r) => r.data));
  }
  getPremiumAussteller(): Observable<PremiumAussteller[]> {
    return this._httpClient
      .get<{ data: PremiumAussteller[] }>(
        `/api/premium-ausstellers`,
        { params: this.commonParams, observe: 'body' }
      )
      .pipe(map((r) => r.data));
  }
  updateAussteller(
    id: number,
    firmenlogoId: number | null,
    logoId: number | null,
    messestand: number,
    // profairs Aussteller
    aussteller: Partial<Aussteller>
  ) {
    return this._httpClient.put(
      `/api/ausstellers/${id}`,
      {
        data: {
          logo: logoId,
          firmenlogo: firmenlogoId,
          messestand,
          firma: aussteller.firma,
          telefon: aussteller.telefon,
          strasse: aussteller.strasse,
          plz: aussteller.plz,
          adresszusatz: aussteller.adresszusatz,
          ort: aussteller.ort,
          land: aussteller.land,
          email: aussteller.email,
          homepage: aussteller.homepage,
        },
      }
    ).pipe(map((response)=>(<any>response)["data"]));
  }
  updateStellenausschreibunegn(
    aussteller: Aussteller,
    logoId: number,
    firmenlogoId: number
  ) {
    return aussteller.stellenausschreibungen.map((s) =>
      lastValueFrom(
        this._httpClient.put(
          `/api/stellenausschreibungen/${s.id}`,
          {
            data: {
              logo: logoId ? logoId : firmenlogoId ? firmenlogoId : null,
            },
          }
        )
      )
    );
  }
  updateOnlinemedien(
    id: number,
    bilderIds: number[],
    farbcode: string,
    videoUrls: string[],
    flyer: number | null,
    unternehmensprofil: number | null,
    text: string,
    socialmedien: string[]
  ) {
    return this._httpClient.put(
      `/api/onlinemediens/${id}`,
      {
        data: {
          bilder: bilderIds,
          farbcode,
          videoUrls,
          unternehmensprofil,
          flyer,
          text,
          socialmedien,
        },
      }
    );
  }
  updateAnsprechpartner(
    id: number,
    ansprechpartner: {
      ausstellerid: number;
      bildId: number;
      telefon: string;
      titel: string;
      vorname: string;
      name: string;
      email: string;
      anrede: string;
    }
  ) {
    return this._httpClient.put(
      `/api/ansprechpartners/${id}`,
      {
        data: {
          ...ansprechpartner,
          aussteller: ansprechpartner.ausstellerid,
          bild: ansprechpartner.bildId,
        },
      }
    );
  }
  getKrankenkasssen(): Observable<Aussteller[]> {
    const params = new HttpParams({
      fromObject: {
        'populate[ausstellers][populate][logo]': '*',
        'populate[ausstellers][populate][firmenlogo]': '*',
      },
    });
    return this._httpClient
      .get<{ data: [{ ausstellers: Aussteller[] }] }>(
        `/api/krankenkassen`,
        { params, observe: 'body' }
      )
      .pipe(map((response) => response.data[0].ausstellers));
  }
}
