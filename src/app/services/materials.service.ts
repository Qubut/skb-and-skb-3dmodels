import { Injectable } from '@angular/core';
import {
  MeshBasicMaterial,
  MeshPhysicalMaterial,
  MeshStandardMaterial,
  Texture,
  Color,
  Side,
} from 'three';

@Injectable({
  providedIn: 'root',
})
export class MaterialsService {
  private _standard = new MeshStandardMaterial();
  private _basic = new MeshBasicMaterial();
  private _physical = new MeshPhysicalMaterial();
  constructor() {}
  getBasic(side:Side, color?: string, texture?: Texture) {
    if (texture) this._basic.map = texture;
    if (color) this._basic.color = new Color(color);
    this._basic.side = side;
    return this._basic;
  }
  getStandard(
    color: string,
    metalness: number,
    roughnes: number,
    side:Side,
    texture?: Texture
  ) {
    this._standard.metalness = metalness;
    this._standard.roughness = roughnes;
    this._standard.side = side;
    if (texture) {
      this._standard.map = texture;
    }
    this._standard.color = new Color(color);
    return this._standard;
  }
  getPhysical(
    color: string,
    clearcoat: number,
    clearcoatRoughness: number,
    side:Side,
    texture?: Texture
  ) {
    let physical = new MeshPhysicalMaterial()
    physical.metalness = clearcoat
    physical.roughness =clearcoatRoughness
    physical.clearcoat = clearcoat;
    physical.clearcoatRoughness = clearcoatRoughness;
    physical.side = side;
    if (texture) {
      physical.map = texture;
    }
    physical.color = new Color(color);
    return physical;
  }
}
