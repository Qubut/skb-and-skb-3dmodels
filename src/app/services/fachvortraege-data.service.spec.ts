import { TestBed } from '@angular/core/testing';

import { FachvortraegeDataService } from './fachvortraege-data.service';

describe('FachvortraegeDataService', () => {
  let service: FachvortraegeDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FachvortraegeDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
