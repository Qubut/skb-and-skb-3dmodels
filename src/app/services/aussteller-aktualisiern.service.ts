import { isPlatformBrowser } from '@angular/common';
import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { lastValueFrom, Observable, of } from 'rxjs';
import { ApiService } from './api.service';
import { DeleteOldFilesService } from './delete-old-files.service';
import { FetchProfairsDataService } from './fetch-profairs-data.service';
import { UploadFilesService } from './upload-files.service';

@Injectable({
  providedIn: 'root',
})
export class AusstellerService {
  private isUpdating = false;
  private mediaIds: { [k: string]: any } = {};
  private data: { [k: string]: any } = {};
  private aussteller!: Aussteller;
  constructor(
    private _fetchProfairsDataService: FetchProfairsDataService,
    private _deleteService: DeleteOldFilesService,
    private _uploadService: UploadFilesService,
    private _apiService: ApiService,
    @Inject(PLATFORM_ID) private platformId: Object
  ) {}

  async updateAussteller(aussteller: Aussteller): Promise<Observable<{updated:boolean;response:Partial<Aussteller>|null}>> {
    this.aussteller = aussteller;
    if (!isPlatformBrowser(this.platformId)) {
      return of({updated:this.isUpdating,response:null});
    }
    try {
      this.isUpdating = true;
      const { data, ausstellerData } = await this.fetchAusstellerData(
        this.aussteller.ausstellerid
      );
      this.data = data;
      await this.deleteMedia();
      this.mediaIds = await this.uploadMedia();
      const { firmenlogoId, logoId } = this.mediaIds;
      const messestand = this.data['messestand'];
     const response = await lastValueFrom(
        this._apiService.updateAussteller(
          this.aussteller.id,
          firmenlogoId,
          logoId,
          messestand == 'Messestand A' ? 1 : 2,
          ausstellerData as Partial<Aussteller>
        )
      );
      await this.updateStellenausschreibungen(logoId, firmenlogoId);
      await this.updateOnlinemedien();
      await this.updateAnsprechpartner();
      return of({updated:true,response});
    } catch (error) {
      console.error(error);
      return of({updated:false,response:null});
    } finally {
      this.isUpdating = false;
    }
  }
  private async updateOnlinemedien() {
    const {
      bilderIds,
      farbcode,
      videoUrls,
      flyerId,
      unternehmensprofilId,
      text,
      socialmedia,
    } = this.mediaIds;
    await lastValueFrom(
      this._apiService.updateOnlinemedien(
        this.aussteller.onlinemedien.id!,
        bilderIds,
        farbcode,
        videoUrls,
        flyerId,
        unternehmensprofilId,
        text,
        socialmedia
      )
    );
  }
  private async updateAnsprechpartner() {
    const { telefon, titel, vorname, name, email, anrede } = this.data[
      'ansprechpartner'
    ] as {
      telefon: string;
      titel: string;
      vorname: string;
      name: string;
      email: string;
      anrede: string;
    };
    const { bildId } = this.mediaIds;
    await lastValueFrom(
      this._apiService.updateAnsprechpartner(
        this.aussteller.ansprechpartner.id!,
        {
          ausstellerid: this.aussteller.id,
          bildId,
          telefon,
          titel,
          vorname,
          name,
          email,
          anrede,
        }
      )
    );
  }
  private async updateStellenausschreibungen(
    logoId: number,
    firmenlogoId: number
  ) {
    await Promise.all(
      this._apiService.updateStellenausschreibunegn(
        this.aussteller,
        logoId,
        firmenlogoId
      )
    );
  }
  private async fetchAusstellerData(ausstellerId: string) {
    const data = await this._fetchProfairsDataService.getData(ausstellerId);
    const {
     firma,
      email,
      telefon,
      homepage,
      land,
      plz,
      ort,
      adresszusatz,
      strasse,
    } = data[0] as Partial<Aussteller>;
    const ausstellerData = {
       firma,
        email,
        telefon,
        homepage,
        plz,
        ort,
        adresszusatz,
        land,
        strasse,
      }
    return {
      data: data[0]['onlinemedien'],
      ausstellerData
    };
  }
  private async deleteMedia() {
    try {
      const media = this.getMediaToDelete();
      const deletePromises = this._deleteService.delete(media);
      await Promise.all(deletePromises);
    } catch (error) {
      console.log(error);
    }
  }

  private getMediaToDelete() {
    const toDelete: number[] = [];
    if (this.aussteller.logo) toDelete.push(this.aussteller.logo.id!);
    if (this.aussteller.firmenlogo)
      toDelete.push(this.aussteller.firmenlogo.id!);
    if (this.aussteller.onlinemedien.bilder)
      this.aussteller.onlinemedien.bilder.forEach((b) => toDelete.push(b.id!));
    if (this.aussteller.ansprechpartner.bild)
      toDelete.push(this.aussteller.ansprechpartner.bild.id!);
    if (this.aussteller.onlinemedien.unternehmensprofil) {
      toDelete.push(this.aussteller.onlinemedien.unternehmensprofil.id!);
    }
    if (this.aussteller.onlinemedien.flyer) {
      toDelete.push(this.aussteller.onlinemedien.flyer.id!);
    }
    return toDelete;
  }

  private async uploadMedia() {
    const {
      farbcode,
      bilder,
      firmenlogo_name,
      firmenlogo,
      logo_name,
      logo,
      text,
      videolink,
      videourl,
      firmenflyer,
      firmenflyer_name,
      file_name,
      file,
      socialmedia_1,
      socialmedia_2,
    } = this.data;
    const { bild, bild_name } = this.data['ansprechpartner'];
    const bilderIds = await this._uploadService.uploadMultipleMedia(bilder);
    const firmenlogoId = await this._uploadService.uploadSingleMedia({
      file_name: firmenlogo_name,
      file: firmenlogo,
    });
    const logoId = await this._uploadService.uploadSingleMedia({
      file_name: logo_name,
      file: logo,
    });
    const videoUrls = [videolink, videourl].filter((v) => v.length);
    const unternehmensprofilId = await this._uploadService.uploadSingleMedia({
      file_name,
      file,
    });
    const flyerId = await this._uploadService.uploadSingleMedia({
      file_name: firmenflyer_name,
      file: firmenflyer,
    });
    const bildId = await this._uploadService.uploadSingleMedia({
      file_name: bild_name,
      file: bild,
    });
    const socialmedia = [socialmedia_1, socialmedia_2];
    return {
      bilderIds,
      flyerId,
      logoId,
      firmenlogoId,
      unternehmensprofilId,
      videoUrls,
      farbcode,
      socialmedia,
      text: text ? text : '',
      bildId,
    };
  }
}
