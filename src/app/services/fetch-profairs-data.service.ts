import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'lodash';
import { lastValueFrom } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class FetchProfairsDataService {
  /**
   * Constructs a new FetchProfairsDataService.
   *
   * @param _http The HttpClient to use for HTTP requests.
   */
  constructor(private _http: HttpClient) {}

  /**
   * Fetches data from the Profairs API for the specified Aussteller ID.
   *
   * @param ausstellerid The ID of the Aussteller to fetch data for.
   * @returns A Promise that resolves to the fetched data.
   */
  getData = (ausstellerid: string) => {
    const params = new HttpParams()
      .set('', 6)
      .set('nocache', '')
      .set('ausstellerid', ausstellerid);
    const url = `https://skb-landshut.profairs.de/users/skb-landshut/schnittstellen/aussteller_json.cfm?messeid=6&ausstellerid=${ausstellerid}&nocache`;
    return lastValueFrom( this._http.get<{[k:string]:any}[]>(url));
  };
}
