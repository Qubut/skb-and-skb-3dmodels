import { Injectable } from '@angular/core';
import { Euler, Vector3 } from 'three';

@Injectable({
  providedIn: 'root',
})
export class ModelDataService {
  private readonly radian = Math.PI / 180;
  private logos: Logos = {
    b: {
      upperRightLogo: {
        rotation: new Euler(0, 0, Math.PI / 2),
        material: {
          side:1,
          clearcoat: 1,
          clearcoatRoughness: 1,
          metalness:1.4,
          roughness: 2,
        },
      },
      upperLeftLogo: {
        rotation: new Euler(0, -Math.PI / 2, Math.PI / 2),
        position: new Vector3(-1.71, 11.1, 14.1),
        material: {
          side:1,
          clearcoat: 1,
          clearcoatRoughness: 0.7,
          metalness: 1.5,
          roughness: 0.7,
        },
      },
      lowerRightLogo: {
        rotation: new Euler(0, 0, Math.PI / 2),
        scale: new Vector3(2.85, 0.4, 3.75),
        position: new Vector3(-10.4, 3.82, -4.19),
        material: {
          side:1,
          clearcoat: 0.1,
          clearcoatRoughness: 1,
          metalness:3.5,
          roughness: 1,
        },
      },
      lowerLeftLogo: {
        rotation: new Euler(0, 0, Math.PI / 2),
        scale: new Vector3(2.85, 0.4, 3.75),
        position: new Vector3(-10.4, 3.82, 6.09),
        material: {
          side:1,
          clearcoat: 0.1,
          clearcoatRoughness: 1,
          metalness:3.3,
          roughness: 1,
        },
      },
    },
    a: {
      upperRightLogo: {
        rotation: new Euler(-0.5 * Math.PI, 0, 0),
        material: {
          side:2,
          metalness:1.5,
          roughness: 1,
          clearcoat: 1,
          clearcoatRoughness: 0.1,
        },
      },
      upperLeftLogo: {
        rotation: new Euler(-0.5 * Math.PI, 0, 0.5 * Math.PI),
        material: {
          side:2,
          metalness:1.5,
          roughness: 1,
          clearcoat: 1,
          clearcoatRoughness: 0.1,
        },
      },
      lowerLeftLogo: {
        rotation: new Euler(-0.5 * Math.PI, 0, 0.5 * Math.PI),
        scale: new Vector3(15, 12, 11),
        material: {
          side:1,
          clearcoat: 0.1,
          clearcoatRoughness: 1,
          metalness:2.5,
          roughness: 1,
        },
      },
      lowerRightLogo: {
        rotation: new Euler(-0.5 * Math.PI, 0, 0.5 * Math.PI),
        scale: new Vector3(15, 12, 11),
        material: {
          side:1,
          clearcoat: 0.1,
          clearcoatRoughness: 1,
          metalness:2.45,
          roughness: 1,
        },
      },
    },
  };
  public readonly tvFrame: BasicDataCollection = {
    a: {
      rotation: new Euler(-Math.PI, 0, -Math.PI),
      scale: new Vector3(2.61, 2.61, 0.89),
      position: new Vector3(9.07, 11.19, -18.9),
    },
    b: {
      rotation: new Euler(-Math.PI, 0, -Math.PI),
      scale: new Vector3(1.39, 1.39, 0.47),
      position: new Vector3(2.76, 5.19, -10.5),
    },
  };
  public readonly tv: BasicDataCollection = {
    a: {
      rotation: new Euler(-Math.PI / 2, 1.5 * Math.PI, Math.PI / 2),
      scale: new Vector3(11.92, 0.38, 5.17),
      position: new Vector3(9.21, 11.21, -18.98),
    },
    b: {
      rotation: new Euler(0, -Math.PI / 2, 180 * this.radian),
      scale: new Vector3(1, 0.2, 2.75),
      position: new Vector3(2.84, 5.2, -10.45),
    },
  };
  public readonly zettel: BasicDataCollection = {
    a: {
      rotation: new Euler(-Math.PI / 2, 0, 0),
      scale: new Vector3(1.03, 4.9, 1.45),
    },
    b: {
      rotation: new Euler(-Math.PI / 2, 0, 0),
      scale: new Vector3(0.49, 2.35, 0.7),
    },
  };
  public readonly pinwallFrame: BasicDataCollection = {
    a: {
      rotation: new Euler(0, -Math.PI / 2, 0),
      scale: new Vector3(0.26, 0.26, 6.77),
    },
    b: {
      rotation: new Euler(0, -Math.PI / 2, 0),
      scale: new Vector3(0.12, 0.12, 3.1),
    },
  };
  public readonly pinwall:BasicDataCollection=
    {
    a: {
      rotation: new Euler(0, -Math.PI/2, 0),
      scale:new Vector3( 0.22, 6.38, 6.38),
    },
    b: {
      rotation: new Euler(0, -Math.PI / 2, 0),
      scale:new Vector3(0.1, 3.06, 3.06),
    },
  };

  constructor() {}
  getLogos() {
    return { b: { ...this.logos.b }, a: { ...this.logos.a } };
  }
}
