import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FakultätenService {

  private readonly _fakultaeten = new BehaviorSubject<Fakultaet[]>([]);
  readonly fakultaeten$: Observable<Fakultaet[]> =
    this._fakultaeten.asObservable();

  addFakultaet(fakultaet: Fakultaet) {
    this._fakultaeten.next([...this._fakultaeten.value, fakultaet]);
  }

  removeFakultaet(fakultaet: Fakultaet) {
    this._fakultaeten.next(
      this._fakultaeten.value.filter((f: Fakultaet) => f !== fakultaet)
    );
  }

  reset() {
    this._fakultaeten.next([]);
  }
}
