import { TestBed } from '@angular/core/testing';

import { DeleteOldFilesService } from './delete-old-files.service';

describe('DeleteOldFilesService', () => {
  let service: DeleteOldFilesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DeleteOldFilesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
