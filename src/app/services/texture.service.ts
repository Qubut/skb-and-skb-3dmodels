import { NgtLoader } from '@angular-three/core';
import { Injectable } from '@angular/core';
import { Observable, of} from 'rxjs';
import { Texture, TextureLoader } from 'three';

@Injectable({
  providedIn: 'root',
})
export class TextureService {
  constructor(private _textureLoader: NgtLoader) {}
  getTexture(texturesUrls: Urls): Observable<Texture | Texture[]> {
  if (!Array.isArray(texturesUrls)) {
    const textureUrl = texturesUrls;
    console.log(textureUrl)
    return  this._textureLoader.use(
          TextureLoader,
          textureUrl
        );


  }
    if (Array.isArray(texturesUrls) && !texturesUrls.length) {
      return of(['', '', '', ''].map((e) => new Texture()));
    }
    if (!texturesUrls.length) {
      return of(new Texture());
    }
    return this._textureLoader.use(TextureLoader, texturesUrls);
  }

}
