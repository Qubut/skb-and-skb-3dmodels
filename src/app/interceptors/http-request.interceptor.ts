import { Injectable } from '@angular/core';
import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import * as LoaderActions from '@skb/stores/loader/loader.actions';
@Injectable()
export class HttpLoadingInterceptor implements HttpInterceptor {
  constructor(private store: Store<{ loader: boolean }>) {}
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    this.store.dispatch(LoaderActions.showLoader());
    return next.handle(request).pipe(
      map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          this.store.dispatch(LoaderActions.hideLoader());
        }
        return event;
      }),
      catchError((error: HttpErrorResponse) => {
        this.store.dispatch(LoaderActions.hideLoader());
        return throwError(error);
      })
    );
  }
}
