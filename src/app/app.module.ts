import { isDevMode, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ErrorPageComponent } from './components/error-page/error-page.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgtSobaLoaderModule } from '@angular-three/soba/loaders';
import { SocialMediaDialogComponent } from './components/dialogs/social-media-dialog/social-media-dialog.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { ImpressumComponent } from './components/impressum/impressum.component';
import { DatenschutzComponent } from './components/datenschutz/datenschutz.component';
import { LoaderComponent } from './components/loader/loader.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MessehalleComponent } from './components/messehalle/messehalle.component';
import { JobwallComponent } from './components/jobwall/jobwall.component';
import { LotteryComponent } from './components/lottery/lottery.component';
import { ExpertPresentationsComponent } from './components/expert-presentations/expert-presentations.component';
import { PresentationsBodyComponent } from './components/expert-presentations/presentations-body/presentations-body.component';
import { KrankenkassenComponent } from './components/krankenkassen/krankenkassen.component';
import { MessebuchComponent } from './components/messebuch/messebuch.component';
import { FirmaprofilComponent } from './components/firmaprofil/firmaprofil.component';
import { FirmaDetailsComponent } from './components/firma-details/firma-details.component';
import { FirmaTableComponent } from './components/firma-table/firma-table.component';
import { MatTabsModule } from '@angular/material/tabs';
import { JobwallDialogComponent } from './components/dialogs/jobwall-dialog/jobwall-dialog.component';
import { GreetingDialogComponent } from './components/dialogs/greeting-dialog/greeting-dialog.component';
import { MediaPlayerDialogComponent } from './components/dialogs/media-player-dialog/media-player-dialog.component';
import { DatenschutzDialogComponent } from './components/dialogs/datenschutz-dialog/datenschutz-dialog.component';
import { SliderDialogComponent } from './components/dialogs/slider/slider-dialog.component';
import { GreetingComponent } from './components/greeting/greeting.component';
import { LinksComponent } from './components/links/links.component';
import { AlumniComponent } from './components/alumni/alumni.component';
import { MessepartnerComponent } from './components/messepartner/messepartner.component';
import { CanvasComponent } from './components/canvas/canvas.component';
import {
  NgtCursorModule,
  NgtPiPipeModule,
  NgtSidePipeModule,
  NgtCanvasModule,
} from '@angular-three/core';
import { NgtMeshModule } from '@angular-three/core/meshes';
import {
  NgtMeshBasicMaterialModule,
  NgtMeshPhysicalMaterialModule,
  NgtMeshStandardMaterialModule,
} from '@angular-three/core/materials';
import { NgtGroupModule } from '@angular-three/core/group';
import { NgtSobaCenterModule, NgtSobaEnvironmentModule } from '@angular-three/soba/staging';
import {NgtSobaOrbitControlsModule} from '@angular-three/soba/controls'
import {
  NgtDirectionalLightModule,
  NgtPointLightModule,
} from '@angular-three/core/lights';
import { MessestandAComponent } from './components/3dModel/messestand-a/messestand-a.component';
import { TableComponent } from './components/3dModel/messestand-a/table/table.component';
import { FloorComponent } from './components/3dModel/messestand-a/floor/floor.component';
import { DeskComponent } from './components/3dModel/messestand-a/desk/desk.component';
import { ZettelComponent } from './components/3dModel/messestand-a/zettel/zettel.component';
import { ChairsComponent } from './components/3dModel/messestand-a/chairs/chairs.component';
import { UpperwallsComponent } from './components/3dModel/messestand-a/walls/upperwalls/upperwalls.component';
import { LowerwallsComponent } from './components/3dModel/messestand-a/walls/lowerwalls/lowerwalls.component';
import { PinwallComponent } from './components/3dModel/messestand-a/pinwall/pinwall.component';
import { TvComponent } from './components/3dModel/messestand-a/tv/tv.component';
import { LogosComponent } from './components/3dModel/messestand-a/logos/logos.component';
import { PalmComponent } from './components/3dModel/messestand-a/palm/palm.component';
import { LightsComponent } from './components/3dModel/messestand-a/lights/lights.component';
import { MessestandBComponent } from './components/3dModel/messestand-b/messestand-b.component';
import { LeftTafelComponent } from './components/3dModel/messestand-a/tafeln/left-tafel/left-tafel.component';
import { RightTafelComponent } from './components/3dModel/messestand-a/tafeln/right-tafel/right-tafel.component';
import { ZettelNumsPipe } from './pipes/zettel-nums.pipe';
import { WallsComponent } from './components/3dModel/messestand-a/walls/walls.component';
import { TafelnComponent } from './components/3dModel/messestand-a/tafeln/tafeln.component';
import { RadianPipe } from './pipes/radian.pipe';
import { WallBComponent } from './components/3dModel/messestand-b/wall/wall.component';
import { FloorBComponent } from './components/3dModel/messestand-b/floor/floor.component';
import { ChairsBComponent } from './components/3dModel/messestand-b/chairs/chairs.component';
import { TableBComponent } from './components/3dModel/messestand-b/table-b/table-b.component';
import { PalmBComponent } from './components/3dModel/messestand-b/palm-b/palm-b.component';
import { CeilingBComponent } from './components/3dModel/messestand-b/ceiling-b/ceiling-b.component';
import { DeskBComponent } from './components/3dModel/messestand-b/desk-b/desk-b.component';
import { TafelnBComponent } from './components/3dModel/messestand-b/tafeln-b/tafeln-b.component';
import { DreiDModelComponent } from './components/3dModel/3d-model.component';
import { LeftTafelBComponent } from './components/3dModel/messestand-b/tafeln-b/left-tafel-b/left-tafel-b.component';
import { RightTafelBComponent } from './components/3dModel/messestand-b/tafeln-b/right-tafel-b/right-tafel-b.component';
import { DatePipe } from './pipes/date.pipe';
import { GetArrayPipe } from './pipes/get-array.pipe';
import { PrefixPipe } from './pipes/prefix.pipe';
import { SearchComponent } from './components/search/search.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { AppRoutingModule } from './app-routing.module';
import { MatDialogModule } from '@angular/material/dialog';
import { MatCommonModule, MatOptionModule } from '@angular/material/core';
import { InfopointComponent } from './components/info-center/infopoint/infopoint.component';
import { VisitorFaqComponent } from './components/info-center/visitor-faq/visitor-faq.component';
import { VisitorSurveyComponent } from './components/info-center/visitor-survey/visitor-survey.component';
import { ExhibitorFaqComponent } from './components/info-center/exhibitor-faq/exhibitor-faq.component';
import { TechnicalSupportComponent } from './components/info-center/technical-support/technical-support.component';
import { CareerServiceComponent } from './components/info-center/career-service/career-service.component';
import { MaphilightModule } from 'ng-maphilight';
import { HomeComponent } from '@skb/components/home/home.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Store, StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import {
  MatPaginatorIntl,
  MatPaginatorModule,
} from '@angular/material/paginator';

import { MatPaginatorTranslator } from './interfaces/mat-paginator-translator';
import { TranslateModule } from '@ngx-translate/core';
import { SafeHtmlPipe } from '@skb/pipes/safe-html.pipe';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { NgxImageGalleryModule } from 'ngx-image-gallery';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { EffectsModule } from '@ngrx/effects';
import { loaderReducer } from './stores/loader/loader.reducer';
import { scrollPositionReducer } from './stores/scroll-position/scroll-position.reducer';
import { HttpLoadingInterceptor } from './interceptors/http-request.interceptor';
import { searchReducer } from './stores/search/search.reducer';
import { SearchEffects } from './stores/search/search.effects';
import { paginatorReducer } from './stores/paginator/paginator.reducer';
import { LetModule } from '@ngrx/component';
import {
  EntityDataModule,
  EntityDataService,
  EntityDefinitionService,
} from '@ngrx/data';
import { entityMetadata } from './app-entity-metadata';
import { HomepageDataService } from './services/homepage-data.service';
import { PaginationComponent } from './components/pagination/pagination.component';
import { FachvortraegeDataService } from './services/fachvortraege-data.service';
import { AusstellerEffects } from './stores/ausstellers/ausstellers.effects';
import { ausstellerReducer } from './stores/ausstellers/ausstellers.reducer';
@NgModule({
  declarations: [
    AppComponent,
    // skb
    SocialMediaDialogComponent,
    ErrorPageComponent,
    NavbarComponent,
    HeaderComponent,
    FooterComponent,
    ImpressumComponent,
    DatenschutzComponent,
    LoaderComponent,
    SearchComponent,
    // companys
    PaginationComponent,
    MessehalleComponent,
    JobwallComponent,
    LotteryComponent,
    ExpertPresentationsComponent,
    PresentationsBodyComponent,
    KrankenkassenComponent,
    MessebuchComponent,
    // firmaprofil
    FirmaprofilComponent,
    FirmaDetailsComponent,
    FirmaTableComponent,
    // Dialogs
    JobwallDialogComponent,
    GreetingDialogComponent,
    MediaPlayerDialogComponent,
    DatenschutzDialogComponent,
    SliderDialogComponent,
    // Home
    HomeComponent,
    GreetingComponent,
    LinksComponent,
    AlumniComponent,
    MessepartnerComponent,
    //3d
    CanvasComponent,
    MessestandAComponent,
    PalmComponent,
    TableComponent,
    FloorComponent,
    DeskComponent,
    ZettelComponent,
    ChairsComponent,
    UpperwallsComponent,
    LowerwallsComponent,
    PinwallComponent,
    TvComponent,
    LogosComponent,
    CanvasComponent,
    LightsComponent,
    MessestandBComponent,
    LeftTafelComponent,
    RightTafelComponent,
    ZettelNumsPipe,
    WallsComponent,
    TafelnComponent,
    RadianPipe,
    WallBComponent,
    FloorBComponent,
    ChairsBComponent,
    TableBComponent,
    PalmBComponent,
    CeilingBComponent,
    DeskBComponent,
    TafelnBComponent,
    DreiDModelComponent,
    LeftTafelBComponent,
    RightTafelBComponent,
    // pipes
    DatePipe,
    GetArrayPipe,
    PrefixPipe,
    RadianPipe,
    ZettelNumsPipe,
    // infopoint
    InfopointComponent,
    VisitorFaqComponent,
    VisitorSurveyComponent,
    ExhibitorFaqComponent,
    TechnicalSupportComponent,
    CareerServiceComponent,
    SafeHtmlPipe,
  ],
  imports: [
    AppRoutingModule,
    BrowserAnimationsModule,
    MaphilightModule,
    NgxImageGalleryModule,
    MatPaginatorModule,
    MatCommonModule,
    MatProgressBarModule,
    MatButtonModule,
    MatIconModule,
    MatDialogModule,
    MatOptionModule,
    MatCheckboxModule,
    MatExpansionModule,
    MatTabsModule,
    MatProgressSpinnerModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    LetModule,
    BrowserModule.withServerTransition({ appId: 'serverApp' }),

    StoreModule.forRoot({
      loader: loaderReducer,
      scrollPosition: scrollPositionReducer,
      search: searchReducer,
      paginator: paginatorReducer,
      ausstellers: ausstellerReducer
    }),
    StoreDevtoolsModule.instrument({
      maxAge: 25, // Retains last 25 states
      logOnly: !isDevMode(), // Restrict extension to log-only mode
      autoPause: true, // Pauses recording actions and state changes when the extension window is not open
      trace: false, //  If set to true, will include stack trace for every dispatched action, so you can see it in trace tab jumping directly to that part of code
      traceLimit: 75, // maximum stack trace frames to be stored (in case trace option was provided as true)
    }),
    EffectsModule.forRoot([SearchEffects,AusstellerEffects]),
    EntityDataModule.forRoot({ entityMetadata }),
    NgtCanvasModule,
    NgtSobaLoaderModule,
    NgtSobaOrbitControlsModule,
    NgtSobaCenterModule,
    NgtPointLightModule,
    NgtDirectionalLightModule,
    NgtMeshModule,
    NgtMeshStandardMaterialModule,
    NgtGroupModule,
    NgtSobaEnvironmentModule,
    NgtCursorModule,
    NgtMeshPhysicalMaterialModule,
    NgtMeshBasicMaterialModule,
    NgtPiPipeModule,
    NgtSidePipeModule,
    TranslateModule.forRoot(),
  ],
  providers: [
    HomepageDataService,
    FachvortraegeDataService,
    EntityDataService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpLoadingInterceptor,
      multi: true,
    },
    {
      provide: MatPaginatorIntl,
      useClass: MatPaginatorTranslator,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
  constructor(
    private entityDefinitionService: EntityDefinitionService,
    private entityDataService: EntityDataService,
    private homepageDataService: HomepageDataService,
    private fachvortraegeService: FachvortraegeDataService
  ) {
    this.entityDefinitionService.registerMetadataMap(entityMetadata);
    this.entityDataService.registerService(
      'Homepage',
      this.homepageDataService
    );
    this.entityDataService.registerService(
      'Fachvortraege',
      this.fachvortraegeService
    );
  }
}
