import { ActionReducerMap } from "@ngrx/store";
import { PaginatorState } from "@skb/interfaces/paginator-state";
import { SearchState } from "@skb/interfaces/searchState";
import { paginatorReducer } from "./paginator/paginator.reducer";
import { searchReducer } from "./search/search.reducer";

interface CombinedState {
  paginator: PaginatorState;
  search: SearchState;
}

export const combinedReducer: ActionReducerMap<CombinedState> = {
  paginator: paginatorReducer,
  search: searchReducer
};
