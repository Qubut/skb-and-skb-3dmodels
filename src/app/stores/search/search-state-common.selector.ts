import { createSelector } from '@ngrx/store';
import { SearchState } from '@skb/interfaces/searchState';
import { PaginatorState } from '@skb/interfaces/paginator-state';

export const searchStateCommonSelector = createSelector(
  (state: { paginator: PaginatorState; search: SearchState }) =>
    state.paginator,
  (state: { paginator: PaginatorState; search: SearchState }) => state.search,
  (paginator, search) => ({ paginator, search })
);
