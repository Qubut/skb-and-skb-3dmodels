import { Action, createReducer, on } from '@ngrx/store';
import { SearchState } from '@skb/interfaces/searchState';
import * as SearchActions from './search.actions';

export const initialSearchState: SearchState = {
  query: '',
  limit: 10,
  offset: 0,
  filter: '',
  indexName: '',
  results: [],
  loading: false,
  error: '',
  total: 0,
};

export const searchReducer = createReducer(
  initialSearchState,
  on(SearchActions.search, (state, { query, limit, offset, filter }) => ({
    ...state,
    query,
    limit: limit ?? state.limit,
    offset: offset ?? state.offset,
    filter: filter ?? state.filter,
    loading: true,
    error: '',
  })),
  on(SearchActions.updateQuery, (state, { query }) => ({ ...state, query })),
  on(SearchActions.resetQuery, (state, { query }) => ({ ...state, query })),
  on(SearchActions.resetFilters, (state, { filters }) => ({ ...state, filters})),
  on(SearchActions.searchSuccess, (state, { results }) => ({
    ...state,
    results,
    loading: false,
    error: '',
  })),
  on(SearchActions.searchFailure, (state, { error }) => ({
    ...state,
    loading: false,
    error,
  }))
);
