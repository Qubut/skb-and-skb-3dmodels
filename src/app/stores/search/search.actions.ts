import { createAction, props } from '@ngrx/store';

export const search = createAction(
  '[Search] Search',
  props<{ query: string; limit: number; offset: number; filter: string; indexName:string }>()
);
export const updateQuery = createAction(
  '[Search] Update Query',
  props<{query:string;}>()
);
export const resetQuery = createAction(
  '[Search] Reset Query',
  props<{query:string;}>()
);
export const resetFilters = createAction(
  '[Search] Reset Filters',
  props<{filters:string;}>()
);
export const searchSuccess = createAction(
  '[Search] Search Success',
  props<{ results: Stellenausschreibung[]|Aussteller[]}>()
);

export const searchFailure = createAction(
  '[Search] Search Failure',
  props<{ error: any }>()
);
