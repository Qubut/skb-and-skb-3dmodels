import { Actions, createEffect, ofType } from '@ngrx/effects';
import { mergeMap } from 'rxjs/operators';
import { SearchService } from '@skb/services/search.service';
import * as SearchActions from './search.actions';
import * as PaginatorActions from '@skb/stores/paginator/paginator.actions';
import { Observable } from 'rxjs';
import { Action, Store } from '@ngrx/store';
import { PaginatorState } from '@skb/interfaces/paginator-state';
import { SearchState } from '@skb/interfaces/searchState';
import { Injectable } from '@angular/core';
import { LRUMap } from 'lru_map';

type SearchResult<TIndexName extends 'aussteller' | 'stellenausschreibung'> =
  TIndexName extends 'aussteller' ? Stellenausschreibung[] : Aussteller[];

@Injectable()
export class SearchEffects {
  private cache = new LRUMap<
    string,
    SearchResult<'aussteller' | 'stellenausschreibung'>
  >(
    1000 // Maximum cache size
  );

  constructor(
    private actions$: Actions,
    private searchService: SearchService,
    private store: Store<{ paginator: PaginatorState; search: SearchState }>
  ) {}

  search$ = createEffect((): Observable<Action> => {
    return <Observable<Action>>this.actions$.pipe(
      ofType(SearchActions.search),
      mergeMap(async ({ query, limit, offset, filter, indexName }) => {
        try {
          if (!this.isIndexName(indexName)) {
            throw new Error(`Invalid index name: ${indexName}`);
          }

          // Check if the search result for the given query parameters is already cached
          const cacheKey = `${query}_${limit}_${offset}_${filter}_${indexName}`;
          const cachedResult = this.cache.get(cacheKey);
          if (cachedResult) {
            const total = await this.searchService.getNbOfEntries(
              indexName,
              query,
              filter
            );
            this.store.dispatch(
              PaginatorActions.setTotalLength({ totalLength: total })
            );
            return SearchActions.searchSuccess({ results: cachedResult });
          }

          const results = <SearchResult<typeof indexName>>(
            await this.searchService.search(
              query,
              limit!,
              offset!,
              filter,
              indexName as 'aussteller' | 'stellenausschreibung'
            )
          );

          // Store the search result in the cache
          this.cache.set(cacheKey, results);

          const total = await this.searchService.getNbOfEntries(
            indexName,
            query,
            filter
          );
          this.store.dispatch(
            PaginatorActions.updateTotalLength({ totalLength: total })
          );

          // Remove the least recently used entry if the cache has reached its maximum size
          if (this.cache.size > this.cache.limit) {
            this.cache.shift();
          }

          return SearchActions.searchSuccess({ results });
        } catch (error) {
          this.store.dispatch(
            PaginatorActions.updateTotalLength({ totalLength: 0 })
          );
          return SearchActions.searchFailure({ error });
        }
      })
    );
  });
  isIndexName = (
    value: string
  ): value is 'aussteller' | 'stellenausschreibung' => {
    return value === 'aussteller' || value === 'stellenausschreibung';
  };
}
