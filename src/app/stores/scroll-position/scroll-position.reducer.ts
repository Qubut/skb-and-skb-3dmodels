import { createReducer, on } from '@ngrx/store';
import { setScrollPosition } from '@skb/stores/scroll-position/scroll-position.action';
import { ScrollPositionState } from '@skb/interfaces/scroll-position-state';

export const initialState: ScrollPositionState = {};

export const scrollPositionReducer = createReducer(
  initialState,
  on(setScrollPosition, (state, { component, position }) => {
    return {
      ...state,
      [component]: position
    };
  })
);
