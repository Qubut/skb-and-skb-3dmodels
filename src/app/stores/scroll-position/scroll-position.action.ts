import { createAction, props } from '@ngrx/store';

export const setScrollPosition = createAction(
  '[Scroll] Set Scroll Position',
  props<{ component: string, position: number }>()
);
