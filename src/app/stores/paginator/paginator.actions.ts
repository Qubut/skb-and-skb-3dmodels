import { createAction, props } from '@ngrx/store';

export const setCurrentPage = createAction(
  '[Paginator] Set Current Page',
  props<{ currentPage: number }>()
);

export const setPageSize = createAction(
  '[Paginator] Set Page Size',
  props<{ pageSize: number }>()
);

export const setTotalLength = createAction(
  '[Paginator] Set Total Length',
  props<{ totalLength: number }>()
);
export const updateTotalLength = createAction(
  '[Paginator] Update Total Length',
  props<{ totalLength: number }>()
);
export const updatePageIndex = createAction(
  '[Paginator] Update Page Index',
  props<{ currentPage: number }>()
);

export const resetPageIndex = createAction(
  '[Paginator] Reset Page Index',
  props<{ currentPage: number }>()
);
export const updatePageSizeOptions = createAction(
  '[Paginator] Update Page Size',
  props<{ pageSizeOptions: number[] }>()
);
