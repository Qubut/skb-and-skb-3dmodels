import { createReducer, on } from '@ngrx/store';
import { PaginatorState } from '@skb/interfaces/paginator-state';
import * as PaginatorActions from './paginator.actions';

const initialState: PaginatorState = {
  currentPage: 0,
  pageSize: 10,
  totalLength: 0,
  pageSizeOptions: [10, 25, 50, 100],
};
export const paginatorReducer = createReducer(
  initialState,
  on(PaginatorActions.setCurrentPage, (state, { currentPage }) => ({
    ...state,
    currentPage,
  })),
  on(PaginatorActions.setPageSize, (state, { pageSize }) => ({
    ...state,
    pageSize,
  })),
  on(PaginatorActions.setTotalLength, (state, { totalLength }) => ({
    ...state,
    totalLength,
  })),
  on(PaginatorActions.updateTotalLength, (state, { totalLength }) => ({
    ...state,
    totalLength,
  })),
  on(PaginatorActions.updatePageSizeOptions, (state, { pageSizeOptions }) => ({
    ...state,
    pageSizeOptions,
  })),
  on(PaginatorActions.updatePageIndex, (state, { currentPage }) => ({
    ...state,
    currentPage,
  })),
  on(PaginatorActions.resetPageIndex, (state, { currentPage }) => ({
    ...state,
    currentPage,
  }))
);
