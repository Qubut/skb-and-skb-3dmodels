import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { AusstellersState } from '@skb/interfaces/ausstellers-state';
import { ApiService } from '@skb/services/api.service';
import { AusstellerService } from '@skb/services/aussteller-aktualisiern.service';
import {
  catchError,
  from,
  map,
  mergeMap,
  of,
  switchMap,
  withLatestFrom,
} from 'rxjs';
import {
  loadAussteller,
  loadAusstellerFail,
  loadAusstellerSuccess,
  updateAussteller,
  updateAusstellerFail,
  updateAusstellerSuccess,
} from './ausstellers.actions';

@Injectable()
export class AusstellerEffects {
  constructor(
    private _apiService: ApiService,
    private _actions$: Actions,
    private _aktualisierenService: AusstellerService,
    private _store: Store<{ ausstellers: AusstellersState }>
  ) {}
  loadAussteller$ = createEffect(() =>
    this._actions$.pipe(
      ofType(loadAussteller),
      withLatestFrom(this._store.select((k) => k.ausstellers.entities)),
      switchMap(([action, entities]) => {
        const { slug } = action;
        const aussteller:Aussteller = entities[slug];
        if (aussteller) {
          return of(loadAusstellerSuccess({ aussteller }));
        } else {
          return this._apiService.getAussteller(slug).pipe(
            map((aussteller) => loadAusstellerSuccess({ aussteller })),
            catchError((error) => of(loadAusstellerFail({ error })))
          );
        }
      })
    )
  );
  updateAussteller$ = createEffect(() =>
    this._actions$.pipe(
      ofType(updateAussteller),
      switchMap(({ aussteller }) =>
        from(this._aktualisierenService.updateAussteller(aussteller)).pipe(
          mergeMap((data) => data),
          map((data) => updateAusstellerSuccess(data)),
          catchError((error) => of(updateAusstellerFail({ error })))
        )
      )
    )
  );
}
