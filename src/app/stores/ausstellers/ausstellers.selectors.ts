import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AusstellersState } from '@skb/interfaces/ausstellers-state';

export const selectAusstellerState =
  createFeatureSelector<AusstellersState>('aussteller');
export const selectAllAussteller = createSelector(
  selectAusstellerState,
  (state) => Object.values(state.entities)
);
export const selectAusstellerBySlug = createSelector(
  (state: { ausstellers: AusstellersState }) => state.ausstellers.entities,
  (_: { ausstellers: AusstellersState }, props: { slug: any }) => props.slug,
  (entities, slug) => entities[slug]
);

export const selectAusstellerLoading = createSelector(
  selectAusstellerState,
  (state) => state.loading
);
export const selectAusstellerLoaded = createSelector(
  selectAusstellerState,
  (state) => state.loaded
);
export const selectAusstellerError = createSelector(
  selectAusstellerState,
  (state) => state.error
);
