import { createAction, props } from '@ngrx/store';

enum AusstellerActionTypes {
  LoadAussteller = '[Aussteller] Load',
  LoadAusstellerSuccess = '[Aussteller] Load Success',
  LoadAusstellerFail = '[Aussteller] Load Fail',
  UpdateAussteller = '[Aussteller] Update',
  UpdateAusstellerSuccess = '[Aussteller] Update Success',
  UpdateAusstellerFail = '[Aussteller] Update Fail',
  SetResponseToNull = '[Aussteller] Response NULL'
}
export const loadAussteller = createAction(
  AusstellerActionTypes.LoadAussteller,
  props<{ slug: string }>()
);
export const loadAusstellerSuccess = createAction(
  AusstellerActionTypes.LoadAusstellerSuccess,
  props<{ aussteller: Aussteller }>()
);
export const loadAusstellerFail = createAction(
  AusstellerActionTypes.LoadAusstellerFail,
  props<{ error: any }>()
);
export const updateAussteller = createAction(
  AusstellerActionTypes.UpdateAussteller,
  props<{ aussteller: Aussteller }>()
);
export const updateAusstellerSuccess = createAction(
  AusstellerActionTypes.UpdateAusstellerSuccess,
  props<{ updated: boolean; response:Partial<Aussteller>| null}>()
);
export const updateAusstellerFail = createAction(
  AusstellerActionTypes.UpdateAusstellerFail,
  props<{ error: any }>()
);
export const setResponseToNull = createAction(AusstellerActionTypes.SetResponseToNull)
