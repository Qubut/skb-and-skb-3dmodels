import { createReducer, on } from '@ngrx/store';
import { AusstellersState } from '@skb/interfaces/ausstellers-state';
import {
  loadAussteller,
  loadAusstellerFail,
  loadAusstellerSuccess,
  setResponseToNull,
  updateAussteller,
  updateAusstellerFail,
  updateAusstellerSuccess,
} from './ausstellers.actions';

const initialState: AusstellersState = {
  entities: {},
  loaded: false,
  loading: false,
  error: null,
  response:null
};
export const ausstellerReducer = createReducer(
  initialState,
  on(loadAussteller, (state) => ({ ...state, loading: true })),
  on(loadAusstellerSuccess, (state, { aussteller }) => ({
    ...state,
    entities: { ...state.entities, [aussteller.slug]: aussteller },
    loaded: true,
    loading: false,
  })),
  on(loadAusstellerFail, (state, { error }) => ({
    ...state,
    error,
    loaded: false,
    loading: false,
  })),
  on(updateAussteller, (state) => ({ ...state, loading: true })),
  on(updateAusstellerSuccess, (state, { response }) => ({
    ...state,
    loading: false,
    response,
  })),
  on(updateAusstellerFail, (state, { error }) => ({
    ...state,
    error,
    loading: false,
  })),
  on(setResponseToNull, (state)=>({...state,response:null}))
);
