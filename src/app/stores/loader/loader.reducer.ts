import { createReducer, on } from '@ngrx/store';
import { hideLoader, showLoader } from '@skb/stores/loader/loader.actions';

export interface LoaderState {
  isLoading: boolean;
}

export const initialState: LoaderState = {
  isLoading: true,
};

export const loaderReducer = createReducer(
  initialState,
  on(showLoader, (state) => ({ ...state, isLoading: true })),
  on(hideLoader, (state) => ({ ...state, isLoading: false }))
);
