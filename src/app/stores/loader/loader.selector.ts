import { createFeatureSelector, createSelector } from '@ngrx/store';
import { LoaderState } from './loader.reducer';

export const getLoaderState = createFeatureSelector<LoaderState>('loader');

export const isLoading = createSelector(
  getLoaderState,
  (state: LoaderState) => state.isLoading
);
