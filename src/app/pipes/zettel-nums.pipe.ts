import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'zettelNums',
})
export class ZettelNumsPipe implements PipeTransform {
  transform(value: number): string[] {
    const randomNumbers = new Set<string>();
    const range = 10;
    while (randomNumbers.size < value) {
      randomNumbers.add(String((~~(Math.random() * range))+1));
    }
    return [...randomNumbers];
  }
}
