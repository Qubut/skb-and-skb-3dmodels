import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'date'
})
export class DatePipe implements PipeTransform {

  transform(value: string): unknown {
    let pieces = value.split(':');
    return (pieces)? `${pieces[0]}:${pieces[1]}`:'';
  }

}
