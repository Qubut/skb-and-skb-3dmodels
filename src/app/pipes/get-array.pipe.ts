import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'getArray',
})
export class GetArrayPipe implements PipeTransform {
  transform(data: Messepartners): Aussteller[] {
    const a = data.ausstellers;
    const pa = data.premium_ausstellers
      ? data.premium_ausstellers
      : data.premium_aussteller;

    return (<Aussteller[]>[])
      .concat(
        a ? (Array.isArray(a) ? a : [a]) : [],
        pa ? (Array.isArray(pa) ? pa : [pa]) : []
      )
      .map((e) => {
        if (e.logo && Array.isArray(e.logo)) return { ...e, logo: e.logo[0] };
        return e;
      });
  }
}
