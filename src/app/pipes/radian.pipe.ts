import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'radian'
})
export class RadianPipe implements PipeTransform {

  transform(value: number): number {

    return (value*Math.PI)/180;
  }

}
