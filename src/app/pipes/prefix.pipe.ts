import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'prefix'
})
export class PrefixPipe implements PipeTransform {

  transform(value: string, args: string): string {
    if(value.startsWith(args)){
      return value
    }
    return `${args}${value.replace( /\s/g, '')}`;
  }

}
