import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CareerServiceComponent } from './components/info-center/career-service/career-service.component';
import { DatenschutzComponent } from './components/datenschutz/datenschutz.component';
import { ErrorPageComponent } from './components/error-page/error-page.component';
import { ExhibitorFaqComponent } from './components/info-center/exhibitor-faq/exhibitor-faq.component';
import { ExpertPresentationsComponent } from './components/expert-presentations/expert-presentations.component';
import { HomeComponent } from './components/home/home.component';
import { ImpressumComponent } from './components/impressum/impressum.component';
import { InfopointComponent } from './components/info-center/infopoint/infopoint.component';
import { JobwallComponent } from './components/jobwall/jobwall.component';
import { KrankenkassenComponent } from './components/krankenkassen/krankenkassen.component';
import { LotteryComponent } from './components/lottery/lottery.component';
import { MessebuchComponent } from './components/messebuch/messebuch.component';
import { MessehalleComponent } from './components/messehalle/messehalle.component';
import { FirmaprofilComponent } from './components/firmaprofil/firmaprofil.component';
import { TechnicalSupportComponent } from './components/info-center/technical-support/technical-support.component';
import { VisitorFaqComponent } from './components/info-center/visitor-faq/visitor-faq.component';
import { VisitorSurveyComponent } from './components/info-center/visitor-survey/visitor-survey.component';

const routes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'Home', component: HomeComponent},
  { path: 'Empfangsbereich', component: InfopointComponent },
  { path: 'Messehalle', component: MessehalleComponent},
  { path: 'Messestand/:slug', component: FirmaprofilComponent},
  { path: 'Messebuch', component: MessebuchComponent},
  { path: 'Jobwall', component: JobwallComponent},
  { path: 'Fachvortraege', component: ExpertPresentationsComponent},
  { path: 'Gewinnspiel', component: LotteryComponent},
  { path: 'Krankenkassen', component: KrankenkassenComponent},
  { path: 'Karriereservice', component: CareerServiceComponent},
  { path: 'AusstellendeFAQs', component: ExhibitorFaqComponent},
  { path: 'BesuchendeFAQs', component: VisitorFaqComponent},
  { path: 'TechnischerSupport', component: TechnicalSupportComponent},
  { path: 'BesucherBefragung', component: VisitorSurveyComponent},
  { path: 'Impressum', component: ImpressumComponent },
  { path: 'Datenschutz', component: DatenschutzComponent },
  { path: '**', component: ErrorPageComponent} // Wildcard route for a 404 page
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabledBlocking'
})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
