
import { DefaultDataServiceConfig, EntityMetadataMap } from '@ngrx/data';

export const defaultDataServiceConfig: DefaultDataServiceConfig = {
  root: '/api',
  timeout: 3000
};
export const entityMetadata: EntityMetadataMap = {
  Homepage: {
    selectId: (homepage: Homepage) => homepage.id,
    entityDispatcherOptions: {
      optimisticAdd: true,
      optimisticUpdate: true,
      optimisticDelete: true,
    },
  },
};
