import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import { lastValueFrom, Observable, Subscription } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { ApiService } from '@skb/services/api.service';
import { remove } from 'lodash';
import { select, Store } from '@ngrx/store';
import { SearchState } from '@skb/interfaces/searchState';
import {
  resetFilters,
  resetQuery,
  search,
  updateQuery,
} from '@skb/stores/search/search.actions';
import { PaginatorState } from '@skb/interfaces/paginator-state';
import { SearchParams } from '@skb/interfaces/searchParams';
import { resetPageIndex } from '@skb/stores/paginator/paginator.actions';
@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit, OnDestroy {
  @Input() isAussteller = false;
  searchForm: UntypedFormGroup;
  fakultaeten$ = new Observable<Fakultaet[]>();
  studiengaenge: string[] = [];
  jobs: string[] = [];
  query = '';
  filters = '';
  subscriptions: Subscription[] = [];
  public jobarten$ = new Observable<Jobart[]>();

  constructor(
    private _formBuilder: UntypedFormBuilder,
    private _apiService: ApiService,
    private _store: Store<{ search: SearchState; paginator: PaginatorState }>
  ) {
    this.searchForm = this._formBuilder.group({
      searchField: [''],
    });
  }
  ngOnInit(): void {
    this.fakultaeten$ = this._apiService.getFakultaeten().pipe(
      map((f) => {
        f.forEach((e) => {
          this.sortFakultätsStudiengänge(e);
        });
        return f.map((e) => {
          return e;
        });
      })
    );
    this.jobarten$ = this._apiService.getJobarten();
    this.subscriptions.push(this._store.subscribe((s) => s.paginator));
    this.subscriptions.push(
      this._store.subscribe((s) => {
        this.query = s.search.query;
      })
    );
    this.dispatchSearch();
  }
  ngOnDestroy(): void {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }
  update(key: string, val: boolean, isAJob: boolean = true) {
    const updateArray = (array: string[], key: string, val: boolean) => {
      if (val) {
        array.push(key);
      } else {
        array = remove(array, (j) => {
          return j !== key;
        });
      }
      return array;
    };

    const promise = new Promise<void>((resolve, _) => {
      if (isAJob) {
        this.jobs = updateArray(this.jobs, key, val);
      } else {
        this.studiengaenge = updateArray(this.studiengaenge, key, val);
      }
      resolve();
    });

    promise
      .then(() => {
        this.setFilters();
        this.dispatchSearch();
      })
      .catch((error) => {
        console.error(error);
      });
  }

  reset(fakultaeten: Fakultaet[], jobarten: Jobart[]) {
    fakultaeten.forEach((f) =>
      f.studiengangs.forEach((s) => (s.checked = false))
    );
    this.studiengaenge.length = 0;
    this.jobs.length = 0;
    jobarten.forEach((j) => {
      j.checked = false;
    });
    this.query = '';
    this.filters = '';
    this._store.dispatch(resetFilters({ filters: this.filters }));
    this._store.dispatch(resetQuery({ query: '' }));
    this.searchForm.reset();
    this.dispatchSearch(true);
  }

  public search(e: Event | SearchParams) {
    if (!this.isSearchParams(e))
      setTimeout(() => {
        this._store.dispatch(
          updateQuery({
            query: <string>(<HTMLInputElement>(<Event>e).target).value.trim(),
          })
        );
        this.dispatchSearch(true, false);
      }, 500);
  }

  async dispatchSearch(typing: boolean = false, reset: boolean = false) {
    const limit = await lastValueFrom(
      this._store.pipe(
        select('paginator'),
        map((p) => {
          return p.pageSize;
        }),
        take(1)
      )
    );
    if (typing) {
      this._store.dispatch(resetPageIndex({ currentPage: 0 }));
    }
    const offset = await lastValueFrom(
      this._store.pipe(
        select('paginator'),
        map((p) => (!reset ? (!typing ? p.currentPage * p.pageSize : 0) : 0)),
        take(1)
      )
    );

    this._store.dispatch(
      search({
        query: this.query,
        limit,
        offset,
        filter: this.filters,
        indexName: this.isAussteller ? 'aussteller' : 'stellenausschreibung',
      })
    );
  }
  sortFakultätsStudiengänge(f: Fakultaet) {
    let bachelors: Studiengang[] = [];
    let masters: Studiengang[] = [];

    f.studiengangs.forEach((s) => {
      if (s.studiengang.includes('MA'))
        masters.push({ studiengang: s.studiengang });
      else bachelors.push({ studiengang: s.studiengang });
      f.studiengangs = bachelors
        .sort((a, b) => (a.studiengang > b.studiengang ? 1 : -1))
        .concat(
          masters.sort((a, b) => (a.studiengang > b.studiengang ? 1 : -1))
        );
    });
  }
  setFilters() {
    let resultString = '';
    if (this.jobs.length) {
      for (let i = 0; i < this.jobs.length; i++) {
        resultString += `art = ${this.jobs[i]}`;
        if (i < this.jobs.length - 1) {
          resultString += ' OR ';
        }
      }
      resultString = `(${resultString})`;
    }
    if (this.studiengaenge.length) {
      if (this.jobs.length) resultString = `${resultString} AND (`;
      else resultString = `(`;
      for (let i = 0; i < this.studiengaenge.length; i++) {
        resultString += `studiengänge = '${this.studiengaenge[i]}'`;
        if (i < this.studiengaenge.length - 1) {
          resultString += ' OR ';
        }
      }
      resultString = `${resultString})`;
    }
    this.filters = resultString;
  }
  isSearchParams(obj: any): obj is SearchParams {
    if (!obj) return false;
    return (
      typeof obj === 'object' &&
      'query' in obj &&
      typeof obj.query === 'string' &&
      'limit' in obj &&
      typeof obj.limit === 'number' &&
      'offset' in obj &&
      typeof obj.offset === 'number'
    );
  }
}
