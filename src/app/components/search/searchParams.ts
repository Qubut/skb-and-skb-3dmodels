interface SearchParams {
  query: string;
  limit: number;
  offset: number;
  filter:any[]
}
