import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, shareReplay, tap } from 'rxjs';
import { ApiService } from '@skb/services/api.service';

@Component({
  selector: 'skb-lottery',
  templateUrl: './lottery.component.html',
  styleUrls: ['./lottery.component.scss'],
})
export class LotteryComponent implements OnInit {
  gewinnspielsmessepartners$ = new Observable<Messepartners>();
  vm$ = new Observable<[Aussteller[], PremiumAussteller[]]>();
  constructor(private _apiService: ApiService, private _router: Router) {}

  ngOnInit(): void {
    this.gewinnspielsmessepartners$ = this._apiService
      .getMessepartners(true)
      .pipe(shareReplay(1))
  }
  public showMessestand(slug: string) {
    if(slug.length)
    this._router.navigate(['Messestand',slug ]);
  }
}
