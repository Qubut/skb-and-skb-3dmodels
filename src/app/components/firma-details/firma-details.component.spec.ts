import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FirmaDetailsComponent } from './firma-details.component';

describe('FirmaDetailsComponent', () => {
  let component: FirmaDetailsComponent;
  let fixture: ComponentFixture<FirmaDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FirmaDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FirmaDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
