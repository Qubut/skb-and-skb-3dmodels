import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'skb-firma-details',
  templateUrl: './firma-details.component.html',
  styleUrls: ['./firma-details.component.scss'],
})
export class FirmaDetailsComponent implements OnInit {
  @Input() aussteller!: Aussteller;
  text = ''
  public config: { [k: string]: boolean | number | string } = {
    fade: true,
    alwaysOn: false,
    neverOn: false,
    fill: true,
    fillColor: '#ffffff',
    fillOpacity: 0.4,
    stroke: true,
    strokeColor: '#000000',
    strokeOpacity: 1,
    strokeWidth: 2,
    shadow: true,
    shadowColor: '#000000',
    shadowOpacity: 0.8,
    shadowRadius: 10,
  };
  constructor() {}

  ngOnInit(): void {
    if(this.aussteller.onlinemedien && this.aussteller.onlinemedien.text)
    this.text = this.aussteller.onlinemedien.text.replace(/\\n/g, '&#10;&#13;');
    this.text = this.text.replace(/\\"/g, '&#34;');
  }
}
