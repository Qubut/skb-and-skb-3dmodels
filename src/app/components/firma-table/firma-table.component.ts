import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'skb-firma-table',
  templateUrl: './firma-table.component.html',
  styleUrls: ['./firma-table.component.scss'],
})
export class FirmaTableComponent implements OnInit {
  mouseOvered: boolean = false;
  onlinemedien?: OnlineMedien;
  hasSocialmedia:boolean = false
  logo:Bild ={url:''};
  firmenlogo:Bild = {url:''};
  @Input() aussteller?: Aussteller;
  colSpan = 3
  constructor() {

  }

  ngOnInit(): void {
   if(this.aussteller){
    if(this.aussteller.onlinemedien){
      this.onlinemedien = this.aussteller.onlinemedien
      this.hasSocialmedia = <boolean> <unknown> Object.keys(this.onlinemedien.socialmedia!).length
     } this.logo = this.aussteller.logo
      this.firmenlogo = this.aussteller.firmenlogo
    }
  }
}
