import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FirmaTableComponent } from './firma-table.component';

describe('FirmaTableComponent', () => {
  let component: FirmaTableComponent;
  let fixture: ComponentFixture<FirmaTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FirmaTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FirmaTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
