import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KrankenkassenComponent } from './krankenkassen.component';

describe('KrankenkassenComponent', () => {
  let component: KrankenkassenComponent;
  let fixture: ComponentFixture<KrankenkassenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KrankenkassenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KrankenkassenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
