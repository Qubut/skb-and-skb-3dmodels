import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '@skb/services/api.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'skb-krankenkassen',
  templateUrl: './krankenkassen.component.html',
  styleUrls: ['./krankenkassen.component.scss']
})
export class KrankenkassenComponent implements OnInit {
  krankenkassen$ = new Observable<Aussteller[]>()
  constructor(private _router: Router,private _apiService:ApiService) { }

  ngOnInit(): void {
    this.krankenkassen$ = this._apiService.getKrankenkasssen()
  }

  public showViewMessestand(slug: string) {
    this._router.navigate(['Messestand', slug]);

  }
}
