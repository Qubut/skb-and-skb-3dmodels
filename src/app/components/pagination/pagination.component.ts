import {
  AfterViewInit,
  Component,
  Input,
  OnDestroy,
  ViewChild,
} from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { Store } from '@ngrx/store';
import { PaginatorService } from '@skb/services/paginator.service';
import { SearchState } from '@skb/interfaces/searchState';
import {
  setPageSize,
  updatePageIndex,
} from '@skb/stores/paginator/paginator.actions';
import { search } from '@skb/stores/search/search.actions';
import { lastValueFrom, map, Observable, Subscription } from 'rxjs';
import { PaginatorState } from '@skb/interfaces/paginator-state';

@Component({
  selector: 'skb-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss'],
})
export class PaginationComponent implements AfterViewInit, OnDestroy {
  search$ = new Observable<SearchState>();
  currentPage = 0;
  pageSize = 0
  @Input() indexName = '';
  @ViewChild(MatPaginator) paginator?: MatPaginator;
  private subscriptions: Subscription[] = [];
  constructor(
    private _store: Store<{
      search: SearchState;
      paginator: PaginatorState;
    }>,
    private _paginatorService: PaginatorService
  ) {}
  ngAfterViewInit(): void {
    this.subscriptions.push(
      this._store
        .select((k) => k.paginator)
        .subscribe((p) => {
          this.currentPage = p.currentPage;
          this.pageSize = p.pageSize
          this.update(this.currentPage + 1,p.totalLength);
        })
    );
  }
  ngOnInit() {
    this.search$ = this._store.select((k) => k.search);
    if (this.paginator) {
      this.paginator._formFieldAppearance;
    }
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }
  async onPageChange(event: PageEvent, s: SearchState) {
    const { pageIndex, pageSize } = event;
    const offset: number = pageIndex * pageSize;
    if(this.currentPage!=pageIndex)
    this._store.dispatch(updatePageIndex({ currentPage: pageIndex }));
    this._store.dispatch(
      search({
        ...s,
        limit: pageSize,
        offset,
        indexName: this.indexName,
      })
    );
    if(this.pageSize!=pageSize)
    this._store.dispatch(setPageSize({ pageSize }));
  }

  get paginator$(): Observable<PaginatorState> {
    return this._paginatorService.paginator$;
  }
  update(pageIndex: number, total:number) {
    const list = document.getElementsByClassName(
      'mat-mdc-paginator-range-label'
    );
    list[0].innerHTML = `Seite: ${pageIndex.toString()} - Trefferzahl: ${total}`;
    if(list && list[1])
    list[1].innerHTML = `Seite: ${pageIndex.toString()} - Trefferzahl: ${total}`;
  }
}
