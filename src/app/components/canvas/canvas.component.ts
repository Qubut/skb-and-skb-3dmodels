import { isPlatformBrowser } from '@angular/common';
import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input,
  PLATFORM_ID,
  Inject,
} from '@angular/core';

@Component({
  selector: 'skb-canvas',
  templateUrl: './canvas.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./canvas.component.scss'],
})
export class CanvasComponent implements OnInit {
  @Input() aussteller!: Aussteller;
  positions: number[] = [30.269, 9.0128, 31.73];
  focus: number = 37.3;
  rotations: number[] = [0, 46.4, 0];
  aspectRatio!: number;
  isA = false;
  sub: any;
  size!: { width: number; height: number };
  constructor(@Inject(PLATFORM_ID) private platformId: Object) {
    if (isPlatformBrowser(this.platformId)) {
      this.aspectRatio = window.innerWidth / window.innerHeight;
      this.size = {
        width: window.innerWidth,
        height: window.innerHeight,
      };
    }
  }
  ngOnInit(): void {
    // screen.orientation.addEventListener('change')
    this.isA = !this.checkMessestandB(this.aussteller!);

    if (this.isA) {
      this.positions = [42.977, 20.1, 40.629];
      this.positions = [0, 0, 0];
      this.rotations = [0, 50, 0];
      this.rotations = [0, 0, 0];
      this.focus = 71;
    }
  }
  checkMessestandB(a: Aussteller) {
    if (!a.messestand) return true;
    return a.messestand.messestand === 'Messestand B';
  }
}
