import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-table-b',
  templateUrl: './table-b.component.html',
  styleUrls: ['./table-b.component.scss'],
})
export class TableBComponent implements OnInit {
  colors = {
    tableTop: 'silver',
    tableLegs: '#040404',
  };
  @Input() model: Nodes = {};
  constructor() {}

  ngOnInit(): void {}
}
