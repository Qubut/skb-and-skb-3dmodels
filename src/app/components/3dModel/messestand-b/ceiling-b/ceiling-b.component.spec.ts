import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CeilingBComponent } from './ceiling-b.component';

describe('CeilingBComponent', () => {
  let component: CeilingBComponent;
  let fixture: ComponentFixture<CeilingBComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CeilingBComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CeilingBComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
