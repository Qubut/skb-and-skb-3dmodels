import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { TextureService } from '@skb/services/texture.service';
import { Mesh } from 'three';

@Component({
  selector: 'app-floor-b',
  templateUrl: './floor.component.html',
  styleUrls: ['./floor.component.scss'],
})
export class FloorBComponent implements OnInit {
  @Input() model: Mesh;
  @Input() url: string = '';
  texture$: Textures = new Observable();
  check = Array.isArray;
  constructor(private _textureLoader: TextureService) {
    this.model = new Mesh();
  }

  ngOnInit(): void {
    this.texture$ = this._textureLoader.getTexture(this.url);
  }
}
