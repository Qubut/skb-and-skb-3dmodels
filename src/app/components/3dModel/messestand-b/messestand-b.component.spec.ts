import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MessestandBComponent } from './messestand-b.component';

describe('MessestandBComponent', () => {
  let component: MessestandBComponent;
  let fixture: ComponentFixture<MessestandBComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MessestandBComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MessestandBComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
