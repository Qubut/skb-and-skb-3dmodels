import { Component, Input, OnInit } from '@angular/core';
import { Mesh } from 'three';

@Component({
  selector: 'app-wall-b',
  templateUrl: './wall.component.html',
  styleUrls: ['./wall.component.scss'],
})
export class WallBComponent implements OnInit {
  @Input() model = new Mesh();
  @Input() color = "red"
  constructor() {}

  ngOnInit(): void {}
}
