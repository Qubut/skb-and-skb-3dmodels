import { Component, Input, OnInit } from '@angular/core';
@Component({
  selector: 'app-messestand-b',
  templateUrl: './messestand-b.component.html',
  styleUrls: ['./messestand-b.component.scss'],
})
export class MessestandBComponent implements OnInit {
  @Input() specifications:{[k:string]:any}={};
  @Input() model: Nodes = {};
  constructor() {}
  ngOnInit(): void {
  }
}
