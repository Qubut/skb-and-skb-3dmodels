import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PalmBComponent } from './palm-b.component';

describe('PalmBComponent', () => {
  let component: PalmBComponent;
  let fixture: ComponentFixture<PalmBComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PalmBComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PalmBComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
