import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-palm-b',
  templateUrl: './palm-b.component.html',
  styleUrls: ['./palm-b.component.scss']
})
export class PalmBComponent implements OnInit {
  @Input() model: Nodes = {};
  colors = {
    leaves: 'green',
    stem: '#008000',
    flowerpot: '#926829',
    soil: '#3e3117',
  };
  constructor() { }

  ngOnInit(): void {
  }

}
