import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TafelnBComponent } from './tafeln-b.component';

describe('TafelnBComponent', () => {
  let component: TafelnBComponent;
  let fixture: ComponentFixture<TafelnBComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TafelnBComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TafelnBComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
