import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-tafeln-b',
  templateUrl: './tafeln-b.component.html',
  styleUrls: ['./tafeln-b.component.scss'],
})
export class TafelnBComponent implements OnInit {
  @Input() model: Nodes = {};
  constructor() {}

  ngOnInit(): void {}
}
