import { Component, Input, OnInit } from '@angular/core';
import { Mesh } from 'three';

@Component({
  selector: 'app-left-tafel-b',
  templateUrl: './left-tafel-b.component.html',
  styleUrls: ['./left-tafel-b.component.scss'],
})
export class LeftTafelBComponent implements OnInit {
  @Input() model = new Mesh();
  hover = false;
  constructor() {}

  ngOnInit(): void {}
}
