import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LeftTafelBComponent } from './left-tafel-b.component';

describe('LeftTafelBComponent', () => {
  let component: LeftTafelBComponent;
  let fixture: ComponentFixture<LeftTafelBComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LeftTafelBComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LeftTafelBComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
