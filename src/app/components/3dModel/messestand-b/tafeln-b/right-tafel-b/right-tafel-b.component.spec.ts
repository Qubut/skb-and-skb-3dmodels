import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RightTafelBComponent } from './right-tafel-b.component';

describe('RightTafelBComponent', () => {
  let component: RightTafelBComponent;
  let fixture: ComponentFixture<RightTafelBComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RightTafelBComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RightTafelBComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
