import { Component, Input, OnInit } from '@angular/core';
import { Mesh } from 'three';

@Component({
  selector: 'app-right-tafel-b',
  templateUrl: './right-tafel-b.component.html',
  styleUrls: ['./right-tafel-b.component.scss'],
})
export class RightTafelBComponent implements OnInit {
  @Input() model = new Mesh();
  hover = false;
  constructor() {}

  ngOnInit(): void {}
}
