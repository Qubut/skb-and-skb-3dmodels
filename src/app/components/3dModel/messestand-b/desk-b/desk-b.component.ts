import { Component, Inject, Input, OnInit, PLATFORM_ID } from '@angular/core';
import { Observable } from 'rxjs';
import { TextureService } from '@skb/services/texture.service';
import { isPlatformBrowser } from '@angular/common';

@Component({
  selector: 'app-desk-b',
  templateUrl: './desk-b.component.html',
  styleUrls: ['./desk-b.component.scss'],
})
export class DeskBComponent implements OnInit {
  colors = { support: '#0a0a0a' };
  @Input() model: Nodes = {};
  @Input() urls: Urls = '';
  check = Array.isArray;
  textures$: Textures = new Observable();
  constructor(
    private _textureLoader: TextureService,
    @Inject(PLATFORM_ID) private platformId: Object
  ) {}

  ngOnInit(): void {
    this.textures$ = this._textureLoader.getTexture(this.urls);
  }
  onClick() {
    if (isPlatformBrowser(this.platformId))
      window.document
        .getElementById('Ausstellertabelle')
        ?.scrollIntoView({ behavior: 'smooth' });
  }
}
