import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeskBComponent } from './desk-b.component';

describe('DeskBComponent', () => {
  let component: DeskBComponent;
  let fixture: ComponentFixture<DeskBComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeskBComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeskBComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
