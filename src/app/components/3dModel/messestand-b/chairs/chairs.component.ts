import { Component, Input, OnInit } from '@angular/core';
import { Mesh } from 'three';

@Component({
  selector: 'app-chairs-b',
  templateUrl: './chairs.component.html',
  styleUrls: ['./chairs.component.scss'],
})
export class ChairsBComponent implements OnInit {
  @Input() model = new Mesh();
  color='#040404'
  constructor() {}

  ngOnInit(): void {}
}
