import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DreiDModelComponent } from './3d-model.component';

describe('DreiDModelComponent', () => {
  let component: DreiDModelComponent;
  let fixture: ComponentFixture<DreiDModelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DreiDModelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DreiDModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
