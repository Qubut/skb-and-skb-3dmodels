import { Component, Input, OnInit } from '@angular/core';
import { NgtGLTFLoader } from '@angular-three/soba/loaders';
import { pluck, shareReplay } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { ModelSpecificationService } from '@skb/services/model-specification.service';
import { Euler, Vector3 } from 'three';
import { VideoLinkService } from '@skb/services/video-link.service';

@Component({
  selector: 'skb-3d-model',
  templateUrl: './3d-model.component.html',
  styleUrls: ['./3d-model.component.scss'],
})
export class DreiDModelComponent implements OnInit {
  messestandType: string = 'b';
  @Input() aussteller!: Aussteller;
  specifications: { [k: string]: any } = {};
  private _nodesUrls: { [k: string]: string };
  model$?: Observable<Nodes>;
  position = new Vector3(0, 0, 0);
  rotation = new Euler(Math.PI / 180, 0, 0);
  model: Nodes = {};
  private _specificationsService?: ModelSpecificationService;
  private _cachedModels: { [k: string]: Observable<Nodes> } = {};

  constructor(
    private _gltfLoader: NgtGLTFLoader,
    private _videoLinkService: VideoLinkService
  ) {
    this._nodesUrls = {
      a: './assets/models/a/modelA.glb',
      b: './assets/models/b/modelB.glb',
    };
  }

  ngOnInit(): void {

    const o = this.aussteller.onlinemedien;
    this.loadModel();
    this.setCoordinates();
    this._specificationsService = new ModelSpecificationService({
      o,
      logo: this.aussteller.logo,
      firmenlogo: this.aussteller.firmenlogo,
      messestand: this.aussteller.messestand
        ? this.aussteller.messestand.messestand
        : 'Messestand B',
    });

    const videos = this._videoLinkService.getVideoUrls(o.videoUrls);
    const thekeplatte: string = this._specificationsService!.thekeplatte;
    const headset: string = './assets/models/textures/headset.png';
    this.specifications = {
      modelType: this.messestandType,
      color: o.farbcode && o.farbcode.length ? o.farbcode : 'white',
      logosUrls: this._specificationsService.logosUrls,
      diskUrls: [thekeplatte, headset],
      videoUrls: videos.videoUrls,
      videoUrlImages: videos.videoUrlImages,
      company: this.aussteller.firma,
      numOfFlyers: this.aussteller.stellenausschreibungen.length,
      firma: this.aussteller.firma,
      bilder: this._specificationsService.Bilder,
      stellenausschreibungen: this.aussteller.stellenausschreibungen,
      scaling: this._specificationsService.scaling,
    };
  }

  checkMessestandB() {
    if (!this.aussteller.messestand) return true;
    return this.aussteller.messestand.messestand === 'Messestand B';
  }

  loadModel() {
    this.messestandType = this.checkMessestandB() ? 'b' : 'a';
    const cachedModel = this._cachedModels[this.messestandType];
    if (cachedModel) {
      this.model$ = cachedModel;
    } else {
      this.model$ = this._gltfLoader
        .load(this._nodesUrls[this.messestandType.toLocaleLowerCase()])
        .pipe(
          pluck('nodes'),
          shareReplay({ bufferSize: 3, refCount: true })
        ) as Observable<Nodes>;
      this._cachedModels[this.messestandType] = this.model$;
    }
  }

  setCoordinates() {
    if (!this.checkMessestandB()) {
      const radian = Math.PI / 180;
      this.messestandType = 'a';
      this.position = new Vector3(5, -10, -45.2);
      this.rotation = new Euler(15 * radian, -75 * radian, -0.000035 * radian);
    }
  }
}
