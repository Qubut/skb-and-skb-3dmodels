import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PinwallComponent } from './pinwall.component';

describe('PinwallComponent', () => {
  let component: PinwallComponent;
  let fixture: ComponentFixture<PinwallComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PinwallComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PinwallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
