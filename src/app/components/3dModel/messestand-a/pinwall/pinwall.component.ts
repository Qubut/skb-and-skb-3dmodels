import { Component, Input, OnInit } from '@angular/core';
import { MatDialog as MatDialog } from '@angular/material/dialog';
import { JobwallDialogComponent } from '@skb/components/dialogs/jobwall-dialog/jobwall-dialog.component';
import { ModelDataService } from '@skb/services/model-data.service';

@Component({
  selector: 'app-pinwall',
  templateUrl: './pinwall.component.html',
  styleUrls: ['./pinwall.component.scss'],
})
export class PinwallComponent implements OnInit {
  @Input() number:number=0
  @Input() modelType: string = 'a';
  @Input() model: Nodes = {};
  @Input() flyersData: {
    firma: string;
    stellenausschreibungen: Stellenausschreibung[];
  } = { firma: '', stellenausschreibungen: [] };
  colors = {
    frame: '#ffffff',
    pinwall: 'grey',
  };
  hover = false;
  pinwall: BasicData;
  pinwallFrame: BasicData;
  constructor(
    private _dialog: MatDialog,
    private _dataService: ModelDataService
  ) {
    this.pinwall = this._dataService.pinwall.a;
    this.pinwallFrame = this._dataService.pinwallFrame.a;
  }

  ngOnInit(): void {
    this.pinwall =
      this.modelType == 'a'
        ? this._dataService.pinwall.a
        : this._dataService.pinwall.b;
    this.pinwallFrame =
      this.modelType == 'a'
        ? this._dataService.pinwallFrame.a
        : this._dataService.pinwallFrame.b;
  }

  openJobwallDialog() {
    this._dialog.open(JobwallDialogComponent, {
      data: this.flyersData,
      width: 'fit-content',
      height: 'fit-content',
    });
  }
}
