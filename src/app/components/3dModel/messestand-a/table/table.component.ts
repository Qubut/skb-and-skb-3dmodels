import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
})
export class TableComponent implements OnInit {
  colors = {
    tableLegs: '#040404',
  };
  @Input() model: Nodes= {};
  constructor() {}

  ngOnInit(): void {}
}
