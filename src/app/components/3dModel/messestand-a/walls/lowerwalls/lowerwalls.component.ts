import { Component, OnInit, Input } from '@angular/core';
import { Mesh } from 'three';

@Component({
  selector: 'app-lowerwalls',
  templateUrl: './lowerwalls.component.html',
  styleUrls: ['./lowerwalls.component.scss'],
})
export class LowerwallsComponent implements OnInit {
  @Input() color: string = 'red';
  @Input() model: Mesh;
  constructor() {
    this.model = new Mesh();
  }
  ngOnInit(): void {}
}
