import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LowerwallsComponent } from './lowerwalls.component';

describe('LowerwallsComponent', () => {
  let component: LowerwallsComponent;
  let fixture: ComponentFixture<LowerwallsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LowerwallsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LowerwallsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
