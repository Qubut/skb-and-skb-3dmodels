import { Component, OnInit, Input } from '@angular/core';
import { Mesh} from 'three';

@Component({
  selector: 'app-upperwalls',
  templateUrl: './upperwalls.component.html',
  styleUrls: ['./upperwalls.component.scss'],
})
export class UpperwallsComponent implements OnInit {
  @Input() model: Mesh;
  color:string = 'white'
  constructor() {
    this.model = new Mesh();
  }

  ngOnInit(): void {}
}
