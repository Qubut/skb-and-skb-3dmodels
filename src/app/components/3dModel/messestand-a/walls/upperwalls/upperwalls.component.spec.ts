import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpperwallsComponent } from './upperwalls.component';

describe('UpperwallsComponent', () => {
  let component: UpperwallsComponent;
  let fixture: ComponentFixture<UpperwallsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpperwallsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpperwallsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
