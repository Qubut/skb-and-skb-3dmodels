import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-walls',
  templateUrl: './walls.component.html',
  styleUrls: ['./walls.component.scss'],
})
export class WallsComponent implements OnInit {
  @Input() model: Nodes = {};
  @Input() color:string = 'red'
  constructor() {}

  ngOnInit(): void {}
}
