import { Component, Input, OnInit } from '@angular/core'
@Component({
  selector: 'app-messestand-a',
  templateUrl: './messestand-a.component.html',
  styleUrls: ['./messestand-a.component.scss'],
})
export class MessestandAComponent implements OnInit {
  @Input() specifications:{[k:string]:any} ={}
  @Input() model: Nodes ={};
  constructor() {}
  ngOnInit(): void {}
}
