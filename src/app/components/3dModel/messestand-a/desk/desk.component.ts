import { Component, Inject, Input, OnInit, PLATFORM_ID } from '@angular/core';
import { Observable } from 'rxjs';
import { TextureService } from '@skb/services/texture.service';
import { MatDialog as MatDialog } from '@angular/material/dialog';
import { isPlatformBrowser } from '@angular/common';
@Component({
  selector: 'app-desk',
  templateUrl: './desk.component.html',
  styleUrls: ['./desk.component.scss'],
})
export class DeskComponent implements OnInit {
  colors = { support: '#0a0a0a' };
  @Input() model: Nodes = {};
  @Input() urls: Urls = '';
  textures$: Textures = new Observable();
  check = Array.isArray;
  constructor(
    private _textureLoader: TextureService,
    public dialog: MatDialog,
    @Inject(PLATFORM_ID) private platformId: Object
  ) {}
  ngOnInit(): void {
    this.textures$ = this._textureLoader.getTexture(this.urls);
  }
  onClick() {
    if (isPlatformBrowser(this.platformId))
      window.document
        .getElementById('Ausstellertabelle')
        ?.scrollIntoView({ behavior: 'smooth' });
  }
  checks(a: any) {
    return this.check(a);
  }
}
