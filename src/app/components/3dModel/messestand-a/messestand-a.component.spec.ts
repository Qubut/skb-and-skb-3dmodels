import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MessestandAComponent } from './messestand-a.component';

describe('ModelAComponent', () => {
  let component: MessestandAComponent;
  let fixture: ComponentFixture<MessestandAComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MessestandAComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MessestandAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
