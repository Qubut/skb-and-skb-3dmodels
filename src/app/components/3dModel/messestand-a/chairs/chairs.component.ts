import { Component, Input, OnInit } from '@angular/core';
import { Texture } from 'three';
@Component({
  selector: 'app-chairs',
  templateUrl: './chairs.component.html',
  styleUrls: ['./chairs.component.scss']
})
export class ChairsComponent implements OnInit {
  @Input() model: Nodes= {};
  @Input() texture =  new Texture()
  // color = '#040404'
  color = '#50392a'
  constructor() { }

  ngOnInit(): void {
  }

}
