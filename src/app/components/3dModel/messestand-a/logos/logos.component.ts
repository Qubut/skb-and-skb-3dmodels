import { Component, Input, OnInit } from '@angular/core';
import { MatDialog as MatDialog } from '@angular/material/dialog';
import { Observable, map } from 'rxjs';
import { SliderDialogComponent } from '@skb/components/dialogs/slider/slider-dialog.component';
import { TextureService } from '@skb/services/texture.service';
import { MeshPhysicalMaterial, MeshStandardMaterial, Texture } from 'three';
import { MaterialsService } from '@skb/services/materials.service';
import { ModelDataService } from '@skb/services/model-data.service';
@Component({
  selector: 'app-logos',
  templateUrl: './logos.component.html',
  styleUrls: ['./logos.component.scss'],
})
export class LogosComponent implements OnInit {
  @Input() scaling: [[number, number], [number, number]] = [
    [1, 1],
    [1, 1],
  ];
  @Input() modelType: string = 'a';
  @Input() model: Nodes = {};
  textures$: Observable<Texture[] | Texture> = new Observable();
  logos: LogosContent
  @Input() sliderData: { firma: string; bilder: Bild[] } = {
    firma: '',
    bilder: [],
  };
  check = Array.isArray;
  @Input() urls: Urls = '';
  lowerLeftLogoM = new MeshPhysicalMaterial();
  lowerRightLogoM = this.lowerLeftLogoM;
  upperLeftLogoM = new MeshStandardMaterial();
  upperRightLogoM = this.upperLeftLogoM;
  constructor(
    private _textureLoader: TextureService,
    private _dialog: MatDialog,
    private _materialService: MaterialsService,
    private _modelDataService: ModelDataService
  ) {
    this.logos = this._modelDataService.getLogos().a

  }

  ngOnInit(): void {
    let data = this._modelDataService.getLogos();
    this.logos = this.modelType == 'a' ? data.a : data.b;
    this.textures$ = this._textureLoader.getTexture(this.urls).pipe(
      map((t) => {
        if (this.check(t)) {
          let material = this.logos.lowerLeftLogo.material;
          this.lowerLeftLogoM =(this.urls[0])? this._materialService.getPhysical(
            'white',
            material.metalness,
            material.roughness,
            material.side,
            t[0]
          ):this._materialService.getPhysical(
            'white',
            material.metalness,
            material.roughness,
            material.side
          );
          if(this.urls[0])
          this.lowerLeftLogoM.envMap = t[0];
          material = this.logos.lowerRightLogo.material
          this.lowerRightLogoM = this._materialService.getPhysical(
            'white',
            material.metalness,
            material.roughness,
            material.side,
            t[1]
          );
          this.lowerRightLogoM.envMap = t[1];
          material = this.logos.upperLeftLogo.material
          this.upperLeftLogoM = this._materialService.getStandard(
            'white',
            material.metalness,
            material.roughness,
            material.side,
            t[2]
          );
          material = this.logos.upperRightLogo.material;
          this.upperRightLogoM = this._materialService.getPhysical(
            'white',
            material.metalness,
            material.roughness,
            material.side,
            t[3]
          );
        }
        return t;
      })
    );
  }
  openSliderDialog() {
    if(this.sliderData.bilder && this.sliderData.bilder[0].url.length)
    this._dialog.open(SliderDialogComponent, {
      data: this.sliderData,
      width: 'fit-content',
      height: 'fit-content',
    });
  }
}
