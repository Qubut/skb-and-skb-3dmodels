import { Component, OnInit, Input } from '@angular/core';
import { MatDialog as MatDialog } from '@angular/material/dialog';
import { Observable} from 'rxjs';
import { MediaPlayerDialogComponent } from '@skb/components/dialogs/media-player-dialog/media-player-dialog.component';
import { ModelDataService } from '@skb/services/model-data.service';
import { TextureService } from '@skb/services/texture.service';
import { Texture } from 'three';

@Component({
  selector: 'app-tv',
  templateUrl: './tv.component.html',
  styleUrls: ['./tv.component.scss'],
})
export class TvComponent implements OnInit {
  texture = new Texture();
  @Input() modelType: string = 'a';
  @Input() model: Nodes = {};
  @Input() videoDialog: { firma: string; videoUrls: string[] } = {
    firma: '',
    videoUrls:[],
  };
  tvFrame: BasicData;
  tv: BasicData;
  hover = false;
  texture$: Observable<Texture[] | Texture> = new Observable();
  check = Array.isArray;
  @Input() urls: string[]=[];
  imageUrl = ''
  constructor(
    private _dialog: MatDialog,
    private _textureLoader: TextureService,
    private _dataService: ModelDataService
  ) {
    this.tvFrame = this._dataService.tvFrame.a;
    this.tv = this._dataService.tv.a;
  }
  ngOnInit(): void {
    this.tvFrame =
      this.modelType == 'a'
        ? this._dataService.tvFrame.a
        : this._dataService.tvFrame.b;
    this.tv =
      this.modelType == 'a' ? this._dataService.tv.a : this._dataService.tv.b;
    this.imageUrl = this.urls.length && this.urls[0].includes(`vi`)?this.urls[0]:(this.urls.length>2)?this.urls[1]:''
    this.texture$ = this._textureLoader.getTexture(this.imageUrl);
  }
  openVideoDialog() {
    this._dialog.open(MediaPlayerDialogComponent, {
      data: this.videoDialog,
      width: '50vw',
      height: 'fit-content',
    });
  }
}
