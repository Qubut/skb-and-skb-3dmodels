import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { ModelDataService } from '@skb/services/model-data.service';
import { TextureService } from '@skb/services/texture.service';
import { Texture } from 'three';

@Component({
  selector: 'app-zettel',
  templateUrl: './zettel.component.html',
  styleUrls: ['./zettel.component.scss'],
})
export class ZettelComponent implements OnInit {
  textures$: Observable<Texture[] | Texture> = new Observable();
  check = Array.isArray;
  @Input() urls: Urls = '';
  @Input() model: Nodes = {};
  @Input('number') zettelNum: number;
  @Input() modelType: string = 'a';
  zettel: BasicData;
  hover = false
  constructor(
    private _textureLoader: TextureService,
    private _dataService: ModelDataService
  ) {
    this.zettelNum = 1;
    this.zettel = this._dataService.zettel.a;
  }
  ngOnInit(): void {
    this.zettel =
      this.modelType == 'a'
        ? this._dataService.zettel.a
        : this._dataService.zettel.b;
    // this.textures$ = this._textureLoader.getTexture(this.urls)
    this.textures$ = this._textureLoader.getTexture(`./assets/models/textures/jobpost.svg`)
  }
}
