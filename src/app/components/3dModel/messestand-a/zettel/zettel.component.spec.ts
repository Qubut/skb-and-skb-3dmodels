import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ZettelComponent } from './zettel.component';

describe('ZettelComponent', () => {
  let component: ZettelComponent;
  let fixture: ComponentFixture<ZettelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ZettelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ZettelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
