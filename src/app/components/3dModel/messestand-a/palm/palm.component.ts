import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-palm',
  templateUrl: './palm.component.html',
  styleUrls: ['./palm.component.scss'],
})
export class PalmComponent implements OnInit {
  @Input() model: Nodes= {};
  colors = {
    leaves: 'green',
    stem: '#008000',
    flowerpot: '#571e05',
    soil: '#3e3117',
  };
  constructor() {}

  ngOnInit(): void {}
}
