import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { TextureService } from '@skb/services/texture.service';
import { Mesh } from 'three';

@Component({
  selector: 'app-floor',
  templateUrl: './floor.component.html',
  styleUrls: ['./floor.component.scss'],
})
export class FloorComponent implements OnInit {
  @Input() model: Mesh;
  @Input() urls: string[] = [];
  textures$: Textures = new Observable();
  check = Array.isArray;
  constructor(private _textureLoader: TextureService) {
    this.model = new Mesh();
  }

  ngOnInit(): void {
    this.textures$ = this._textureLoader.getTexture(this.urls);
  }
}
