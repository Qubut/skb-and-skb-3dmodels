import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LeftTafelComponent } from './left-tafel.component';

describe('LeftTafelComponent', () => {
  let component: LeftTafelComponent;
  let fixture: ComponentFixture<LeftTafelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LeftTafelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LeftTafelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
