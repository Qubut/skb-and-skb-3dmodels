import { Component, OnInit, Input } from '@angular/core';
import { Mesh } from 'three';

@Component({
  selector: 'app-left-tafel',
  templateUrl: './left-tafel.component.html',
  styleUrls: ['./left-tafel.component.scss'],
})
export class LeftTafelComponent implements OnInit {
  hover = false;
  @Input() model: Mesh;
  constructor() {
    this.model = new Mesh();
  }
  ngOnInit(): void {}
}
