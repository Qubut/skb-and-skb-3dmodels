import { Component, OnInit, Input } from '@angular/core';
import { Mesh } from 'three';

@Component({
  selector: 'app-right-tafel',
  templateUrl: './right-tafel.component.html',
  styleUrls: ['./right-tafel.component.scss'],
})
export class RightTafelComponent implements OnInit {
  hover = false;
  @Input() model: Mesh;
  constructor() {
    this.model = new Mesh();
  }
  ngOnInit(): void {}
}
