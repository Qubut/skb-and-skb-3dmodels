import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RightTafelComponent } from './right-tafel.component';

describe('RightTafelComponent', () => {
  let component: RightTafelComponent;
  let fixture: ComponentFixture<RightTafelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RightTafelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RightTafelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
