import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TafelnComponent } from './tafeln.component';

describe('TafelnComponent', () => {
  let component: TafelnComponent;
  let fixture: ComponentFixture<TafelnComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TafelnComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TafelnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
