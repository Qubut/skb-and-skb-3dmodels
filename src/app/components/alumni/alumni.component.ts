import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'skb-alumni',
  templateUrl: './alumni.component.html',
  styleUrls: ['./alumni.component.scss']
})
export class AlumniComponent implements OnInit {

  @Input() alumniTitle:string= "";
  @Input() alumnistories:Alumnistory[] =[]
  constructor() { }

  ngOnInit(): void {
  }

}
