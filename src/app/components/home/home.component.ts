import { Component, OnInit } from '@angular/core';
import { map, Observable } from 'rxjs';
import { Homepage } from '@skb/interfaces/homepage';
import { HomepageDataService } from '@skb/services/homepage-data.service';

@Component({
  selector: 'skb-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  public greetingTitle: string = 'Greeting Card';
  public messepartnerTitle: string = 'Messepartner';
  public alumniTitle: string = 'Alumni Stories';
  public linksTitle: string = 'LinksComponent';
  homepage$ = new Observable<Homepage>();
  constructor(private _homepageService: HomepageDataService) {}

  ngOnInit() {
    this.homepage$ = this._homepageService
      .getAll()
      .pipe(map((r) => <Homepage>(<any>r)));
  }
}
