import { isPlatformBrowser } from '@angular/common';
import { Component, Inject, Input, OnInit, PLATFORM_ID } from '@angular/core';
import { MatDialog as MatDialog } from '@angular/material/dialog';
import { MediaPlayerDialogComponent } from '@skb/components/dialogs/media-player-dialog/media-player-dialog.component';

@Component({
  selector: 'skb-greeting',
  templateUrl: './greeting.component.html',
  styleUrls: ['./greeting.component.scss'],
})
export class GreetingComponent implements OnInit {
  @Input() greetingTitle: string = '';
  @Input() homepage!: Homepage;
  constructor(private _dialog: MatDialog,

    @Inject(PLATFORM_ID) private platformId: Object
    ) {}

  ngOnInit(): void {
    this.homepage.begruessung = this.homepage.begruessung.replace(/\\n/g, '&#10;&#13;')
  }

  openDialog(item: any): void {
    if(isPlatformBrowser(this.platformId)){
    if (!item['file'] && item['video']) {
      let data = {
        title: item['ueberschrift'],
        videoUrls: [item['video']],
        firma: '',
      };
      this._dialog.open(MediaPlayerDialogComponent, {
        //width: "60%",
        data,
        width: '600px',
        //panelClass: --> Costumized
        height: 'fit-content',
      });
    } else if (item['file'] && item['file']['url']) {
      window.open(item['file']['url'], '_blank');
    }
  }
}
}
