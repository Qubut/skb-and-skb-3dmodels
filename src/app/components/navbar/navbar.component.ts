import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, tap } from 'rxjs';
import { ApiService } from '@skb/services/api.service';

@Component({
  selector: 'skb-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  public mobile: boolean = false;
  items$ = new Observable<Item[]>();
  items: Item[] = [];
  constructor(
    public activatedRoute: ActivatedRoute,
    public router: Router,
    private _apiService: ApiService
  ) {}

  ngOnInit(): void {
    this.items$ = this._apiService
      .getNavigationsleiste()
  }
  hasChild(i: number, items: Item[]): boolean {
    return items[i].children.length ? true : false;
  }
  isActive(url: string, children: Item[]): boolean {
    return children.some((child) => url == child.url);
  }
}
