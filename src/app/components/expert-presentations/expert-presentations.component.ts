import { Component, OnInit } from '@angular/core';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { map, Observable } from 'rxjs';
import { FachvortraegeDataService } from '@skb/services/fachvortraege-data.service';

@Component({
  selector: 'skb-expert-presentations',
  templateUrl: './expert-presentations.component.html',
  styleUrls: ['./expert-presentations.component.scss'],
})
export class ExpertPresentationsComponent implements OnInit {
  public index: number = 0;

  fachvortraege$ = new Observable<{ [key: string]: Fachvortrag[] }>();
  isLoading$ = new Observable<boolean>();
  constructor(private _fachvortraegeDataService: FachvortraegeDataService) {}

  ngOnInit(): void {
    this.fachvortraege$ = this._fachvortraegeDataService
      .getAll()
      .pipe(map((r) => <{ [key: string]: Fachvortrag[] }>(<unknown>r)));
  }

  public tabChanged(tabChangeEvent: MatTabChangeEvent) {
    this.index = tabChangeEvent.index;
  }
}
