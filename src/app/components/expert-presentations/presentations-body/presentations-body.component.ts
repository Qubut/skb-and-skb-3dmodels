import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'skb-presentations-body',
  templateUrl: './presentations-body.component.html',
  styleUrls: ['./presentations-body.component.scss'],
})
export class PresentationsBodyComponent implements OnInit {
  @Input() firstDayPresentationTitle: string = '';
  @Input() fachvortraege: Fachvortrag[] = [];
  constructor() {}

  ngOnInit(): void {
  }
}
