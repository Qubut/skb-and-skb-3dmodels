import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PresentationsBodyComponent } from './presentations-body.component';

describe('PresentationsBodyComponent', () => {
  let component: PresentationsBodyComponent;
  let fixture: ComponentFixture<PresentationsBodyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PresentationsBodyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PresentationsBodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
