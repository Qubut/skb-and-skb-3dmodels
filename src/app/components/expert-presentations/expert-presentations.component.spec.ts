import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpertPresentationsComponent } from './expert-presentations.component';

describe('ExpertPresentationsComponent', () => {
  let component: ExpertPresentationsComponent;
  let fixture: ComponentFixture<ExpertPresentationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExpertPresentationsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpertPresentationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
