import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MessebuchComponent } from './messebuch.component';

describe('MessebuchComponent', () => {
  let component: MessebuchComponent;
  let fixture: ComponentFixture<MessebuchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MessebuchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MessebuchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
