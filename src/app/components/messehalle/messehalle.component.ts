import { AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { UntypedFormGroup, UntypedFormBuilder } from '@angular/forms';
import { map, Observable, Subscription } from 'rxjs';
import { LoaderState } from '@skb/stores/loader/loader.reducer';
import { select, Store } from '@ngrx/store';
import { isLoading } from '@skb/stores/loader/loader.selector';
import { SearchState } from '@skb/interfaces/searchState';

@Component({
  selector: 'skb-messehalle',
  templateUrl: './messehalle.component.html',
  styleUrls: ['./messehalle.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MessehalleComponent implements OnInit, AfterViewInit {
  public searchForm: UntypedFormGroup;
  public isLoading$ = new Observable<boolean>();
  ausstellers$ = new Observable<Aussteller[]>();
  constructor(
    private formBuilder: UntypedFormBuilder,
    private _store: Store<{
      loader: LoaderState;
      search: SearchState;
    }>,
    private _cdr: ChangeDetectorRef
  ) {
    this.searchForm = this.formBuilder.group({
      company: [''],
    });
  }
  ngAfterViewInit(): void {
    this._cdr.detectChanges()
  }
  ngOnInit() {
    this.isLoading$ = this._store.select((k)=>k.search.loading);
    this.ausstellers$ = this._store.pipe(
      select('search'),
      map((s) => <Aussteller[]>s.results)
    );
  }
  getRouting = (slug: string) => `/Messestand/${slug}`;
  find(aussteller: Aussteller): string {
    return aussteller.slug;
  }
}
