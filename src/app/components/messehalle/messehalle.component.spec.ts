import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MessehalleComponent } from './messehalle.component';

describe('MessehalleComponent', () => {
  let component: MessehalleComponent;
  let fixture: ComponentFixture<MessehalleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MessehalleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MessehalleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
