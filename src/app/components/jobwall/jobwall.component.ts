import { Component, OnInit, ViewChild } from '@angular/core';
import { delay, map, Observable, retry } from 'rxjs';
import { Location } from '@angular/common';
import { SearchComponent } from '../search/search.component';
import { select, Store } from '@ngrx/store';
import { LoaderState } from '@skb/stores/loader/loader.reducer';
import { isLoading } from '@skb/stores/loader/loader.selector';
import { SearchState } from '@skb/interfaces/searchState';

@Component({
  selector: 'skb-jobwall',
  templateUrl: './jobwall.component.html',
  styleUrls: ['./jobwall.component.scss'],
})
export class JobwallComponent implements OnInit {
  @ViewChild('search') searchComponent?: SearchComponent;
  data: Stellenausschreibung[] = [];
  jobs: string[] = [];
  studiengaenge: string[] = [];
  stellenausschreibungen: Stellenausschreibung[] = [];
  public isLoading$ = new Observable<boolean>();
  searchFired = false;
  loaded = false;
  offset = 0;
  stellenausschreibungen$ = new Observable<Stellenausschreibung[]>();
  constructor(
    private _store: Store<{
      loader: LoaderState;
      search: SearchState;
    }>
  ) {}

  async ngOnInit(): Promise<void> {
    this.isLoading$ = this._store.pipe(map((status)=>{
      this.loaded = !status.search.loading
      return ((!this.loaded) || status.loader.isLoading)
    }));
    this.stellenausschreibungen$ = this._store.pipe(
      select('search'),
      map((s) => {
        return <Stellenausschreibung[]>s.results;
      })
    );
  }
  async istDataVorhanden(length: number): Promise<boolean> {
    await new Promise(resolve => setTimeout(resolve, 1000));
    return !!length;
  }

}
