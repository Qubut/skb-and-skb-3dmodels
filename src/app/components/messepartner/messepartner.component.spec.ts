import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MessepartnerComponent } from './messepartner.component';

describe('MessepartnerComponent', () => {
  let component: MessepartnerComponent;
  let fixture: ComponentFixture<MessepartnerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MessepartnerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MessepartnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
