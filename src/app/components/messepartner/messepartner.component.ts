import { Component, OnInit, Input } from '@angular/core';
import { ApiService } from '@skb/services/api.service';
import { Router } from '@angular/router';
import { Observable, tap } from 'rxjs';

@Component({
  selector: 'skb-messepartner',
  templateUrl: './messepartner.component.html',
  styleUrls: ['./messepartner.component.scss'],
})
export class MessepartnerComponent implements OnInit {
  @Input() public messepartnerTitle: string = '';
  constructor(private _router: Router, private _apiService: ApiService) {}
  messepartners$ = new Observable<Messepartners>();

  ngOnInit(): void {
    this.messepartners$ = this._apiService.getMessepartners();
  }

  public showViewMessestand(slug: string) {
    if (slug.length) this._router.navigate(['Messestand', slug]);
  }
}
