import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisitorSurveyComponent } from './visitor-survey.component';

describe('VisitorSurveyComponent', () => {
  let component: VisitorSurveyComponent;
  let fixture: ComponentFixture<VisitorSurveyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VisitorSurveyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VisitorSurveyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
