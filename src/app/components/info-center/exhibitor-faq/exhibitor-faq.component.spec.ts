import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExhibitorFaqComponent } from './exhibitor-faq.component';

describe('ExhibitorFaqComponent', () => {
  let component: ExhibitorFaqComponent;
  let fixture: ComponentFixture<ExhibitorFaqComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExhibitorFaqComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExhibitorFaqComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
