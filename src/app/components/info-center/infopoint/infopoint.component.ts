import { Component, Inject, OnInit, PLATFORM_ID } from '@angular/core';
import { MatDialog as MatDialog } from '@angular/material/dialog';
import { GreetingDialogComponent } from '../../dialogs/greeting-dialog/greeting-dialog.component';
// import 'image-map-resizer';
import { isPlatformBrowser } from '@angular/common';

declare function imageMapResize(): void;

@Component({
  selector: 'skb-infopoint',
  templateUrl: './infopoint.component.html',
  styleUrls: ['./infopoint.component.scss'],
})
export class InfopointComponent implements OnInit {
  public config: { [k: string]: boolean | string | number } = {
    fade: true,
    alwaysOn: false,
    neverOn: false,
    fill: true,
    fillColor: '#ffffff',
    fillOpacity: 0.4,
    stroke: true,
    strokeColor: '#000000',
    strokeOpacity: 1,
    strokeWidth: 2,
    shadow: true,
    shadowColor: '#000000',
    shadowOpacity: 0.8,
    shadowRadius: 10,
  };

  constructor(
    public dialog: MatDialog,
    @Inject(PLATFORM_ID) private platformId: Object
  ) {}

  ngOnInit(): void {}

  imageResized() {
    if (isPlatformBrowser(this.platformId)) {
      // imageMapResize(); // Javascript function in imageMapResizer.min.js
      // causes errors upon using Server Side Rendering SSR
    }
  }

  openDialog(title: string, mediaUrl: string): void {
    let data = { title: title, url: mediaUrl };

    const dialogRef = this.dialog.open(GreetingDialogComponent, {
      width: '60%',
      data: data,
      //panelClass: --> Costumized
    });
  }
}
