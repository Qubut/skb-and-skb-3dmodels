import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InfopointComponent } from './infopoint.component';

describe('InfopointComponent', () => {
  let component: InfopointComponent;
  let fixture: ComponentFixture<InfopointComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InfopointComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InfopointComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
