import { Component, OnInit } from '@angular/core';
import { ApiService } from '@skb/services/api.service';

//Karriereservice: AusstellerId 731, UnternehmensId 90

@Component({
  selector: 'skb-career-service',
  templateUrl: './career-service.component.html',
  styleUrls: ['./career-service.component.scss'],
})
export class CareerServiceComponent implements OnInit {
  constructor(private _apiService: ApiService) {}

  public unternehmen: any;
  public errorMessage: string = '';
  public hasData: boolean = false;

  ngOnInit(): void {}
}
