import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'skb-links',
  templateUrl: './links.component.html',
  styleUrls: ['./links.component.scss'],
})
export class LinksComponent implements OnInit {
  @Input() public linksTitle: string = '';
  @Input() public infokarten: Infokarte[] = [];
  @Input() public homepage!: Homepage;
  constructor(private _router: Router) {}

  ngOnInit(): void {}
  hasLogo() {
    return this.homepage.skblogo;
  }
  public showMessestand(slug: string) {

    this._router.navigate(['Messestand', slug]);
  }
}
