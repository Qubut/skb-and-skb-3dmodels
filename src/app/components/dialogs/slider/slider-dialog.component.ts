import {
  Component,
  OnInit,
  Inject,
  ViewChild,
  AfterViewInit,
  ChangeDetectionStrategy,
} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {
  NgxImageGalleryComponent,
  GALLERY_IMAGE,
  GALLERY_CONF,
} from 'ngx-image-gallery';
@Component({
  selector: 'skb-slider-dialog',
  templateUrl: './slider-dialog.component.html',
  styleUrls: ['./slider-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SliderDialogComponent implements OnInit, AfterViewInit {
  @ViewChild(NgxImageGalleryComponent)
  ngxImageGallery?: NgxImageGalleryComponent;
  bilder: GALLERY_IMAGE[] = [];
  conf: GALLERY_CONF = {
    imageOffset: '0px',
    showDeleteControl: false,
    showImageTitle: false,
  };
  constructor(
    public dialogRef: MatDialogRef<SliderDialogComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: { firma: string; bilder: Bild[] }
  ) {}
  ngOnInit(): void {
    this.bilder = this.data.bilder.map<GALLERY_IMAGE>(
      (b) =>
        <GALLERY_IMAGE>{
          url: b.url,
          altText: b.name,
          title: b.name,
          thumbnailUrl: b.url,
        }
    );
  }
  ngAfterViewInit(): void {
    setTimeout(() => {
      this.openGallery();
    });
  }

  // open gallery
  openGallery(index: number = 0) {
    this.ngxImageGallery!.open(index);
  }

  // close gallery
  closeGallery() {
    this.ngxImageGallery!.close();

  }

  // set new active(visible) image in gallery
  newImage(index: number = 0) {
    this.ngxImageGallery!.setActiveImage(index);
  }

  // next image in gallery
  nextImage(index: number = 0) {
    this.ngxImageGallery!.next();
  }

  // prev image in gallery
  prevImage(index: number = 0) {
    this.ngxImageGallery!.prev();
  }

  /**************************************************/

  // EVENTS
  // callback on gallery opened
  galleryOpened(index: number) {

  }

  // callback on gallery closed
  galleryClosed() {
    this.dialogRef.close();

  }

  // callback on gallery image clicked
  galleryImageClicked(index: number) {

  }

  // callback on gallery image changed
  galleryImageChanged(index: number) {

  }

  // callback on user clicked delete button
  deleteImage(index: number) {

  }
  onClick() {
    this.dialogRef.close();
  }
}
