import { isPlatformBrowser } from '@angular/common';
import { Component, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'skb-datenschutz-dialog',
  templateUrl: './datenschutz-dialog.component.html',
  styleUrls: ['./datenschutz-dialog.component.scss']
})
export class DatenschutzDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<DatenschutzDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public media: string, @Inject(PLATFORM_ID) private platformId: Object) { }
  ngOnInit(): void {
  }

  public openMedia(){
    if (isPlatformBrowser(this.platformId))
    window.open(this.media);
    this.dialogRef.close();
  }

  public onNoClick() {
    this.dialogRef.close();
  }

}
