import { isPlatformBrowser } from '@angular/common';
import { Component, Inject, OnInit, PLATFORM_ID } from '@angular/core';
import {  MatDialogRef,  MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'skb-social-media-dialog',
  templateUrl: './social-media-dialog.component.html',
  styleUrls: ['./social-media-dialog.component.scss']
})
export class SocialMediaDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<SocialMediaDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public media: string,
     @Inject(PLATFORM_ID) private platformId: Object) { }

  ngOnInit(): void {
  }

  goToUrlFacebook() {
    if (isPlatformBrowser(this.platformId))
    window.open('https://www.facebook.com/SKB.Landshut/');
  }

  goToUrlInstagramm() {
    if (isPlatformBrowser(this.platformId))
    window.open('https://www.instagram.com/skb_landshut/');
  }

  onNoClick(){
    this.dialogRef.close();
  }


}
