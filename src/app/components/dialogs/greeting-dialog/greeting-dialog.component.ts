import { Component, OnInit, Inject } from '@angular/core';
import {  MatDialogRef,  MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'skb-greeting-dialog',
  templateUrl: './greeting-dialog.component.html',
  styleUrls: ['./greeting-dialog.component.scss']
})
export class GreetingDialogComponent implements OnInit {

  public title:string = '';
  public mediaUrl: string = '';
  public safeSrc: SafeResourceUrl = '';

  constructor (public dialogRef: MatDialogRef<GreetingDialogComponent>,
  @Inject (MAT_DIALOG_DATA) public data: any,
  private sanitizer: DomSanitizer) { }

  ngOnInit(): void {
    this.title = this.data.title;
    this.mediaUrl = this.data.url;
    this.safeSrc =  this.sanitizer.bypassSecurityTrustResourceUrl(this.mediaUrl);
  }

  onNoClick(){
    this.dialogRef.close();
  }

}
