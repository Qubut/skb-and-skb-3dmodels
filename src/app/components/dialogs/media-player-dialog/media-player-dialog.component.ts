import { Component, Inject, OnInit } from '@angular/core';
import {  MatDialogRef,  MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'skb-media-player-dialog',
  templateUrl: './media-player-dialog.component.html',
  styleUrls: ['./media-player-dialog.component.scss'],
})
export class MediaPlayerDialogComponent implements OnInit {
  public title: string = '';
  public mediaUrl: string = '';
  public safeSrc: SafeResourceUrl = '';

  constructor(
    public dialogRef: MatDialogRef<MediaPlayerDialogComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: {
      title: string;
      url: string;
      firma: string;
      videoUrls: string[];
    },
    private sanitizer: DomSanitizer
  ) {}
  urls: string[] = [];
  mediaUrls: string[] = [];
  safeSrcs: SafeResourceUrl[] = [];
  ngOnInit(): void {
    this.mediaUrls = this.data.videoUrls.map((u) =>
      u.length ? u.replace('watch?v=', 'embed/') : u
    );
    this.safeSrcs = this.mediaUrls
      .filter((e) => e.length)
      .map((u) => this.sanitizer.bypassSecurityTrustResourceUrl(u));
    this.data.title = this.data.firma ? this.data.firma : this.data.title;
    this.mediaUrl = this.data.url ? this.data.url : '';
    this.safeSrc = this.sanitizer.bypassSecurityTrustResourceUrl(this.mediaUrl);
  }

  onNoClick() {
    this.dialogRef.close();
  }
}
