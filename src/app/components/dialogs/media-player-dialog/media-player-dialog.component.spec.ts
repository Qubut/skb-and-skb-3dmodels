import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MediaPlayerDialogComponent } from './media-player-dialog.component';

describe('MediaPlayerDialogComponent', () => {
  let component: MediaPlayerDialogComponent;
  let fixture: ComponentFixture<MediaPlayerDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MediaPlayerDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MediaPlayerDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
