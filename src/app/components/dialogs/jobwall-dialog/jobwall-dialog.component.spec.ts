import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JobwallDialogComponent } from './jobwall-dialog.component';

describe('JobwallDialogComponent', () => {
  let component: JobwallDialogComponent;
  let fixture: ComponentFixture<JobwallDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JobwallDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JobwallDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
