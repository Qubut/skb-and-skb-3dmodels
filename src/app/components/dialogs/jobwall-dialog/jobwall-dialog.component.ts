import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'skb-jobwall-dialog',
  templateUrl: './jobwall-dialog.component.html',
  styleUrls: ['./jobwall-dialog.component.scss']
})
export class JobwallDialogComponent implements OnInit {



  constructor(public dialogRef: MatDialogRef<JobwallDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: {firma:string,stellenausschreibungen:Stellenausschreibung[]}) { }

  ngOnInit(): void {

  }


  public onClick() {
    this.dialogRef.close();
  }
}
