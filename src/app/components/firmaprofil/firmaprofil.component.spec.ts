import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FirmaprofilComponent } from './firmaprofil.component';

describe('FirmaprofilComponent', () => {
  let component: FirmaprofilComponent;
  let fixture: ComponentFixture<FirmaprofilComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FirmaprofilComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FirmaprofilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
