import { Component, Inject, OnInit, PLATFORM_ID } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { tap,filter, Observable } from 'rxjs';
import { select, Store } from '@ngrx/store';
import {
  loadAussteller,
  setResponseToNull,
  updateAussteller,
} from '@skb/stores/ausstellers/ausstellers.actions';
import { AusstellersState } from '@skb/interfaces/ausstellers-state';
import { selectAusstellerBySlug } from '@skb/stores/ausstellers/ausstellers.selectors';
import { isPlatformBrowser } from '@angular/common';

@Component({
  selector: 'skb-firmaprofil',
  templateUrl: './firmaprofil.component.html',
  styleUrls: ['./firmaprofil.component.scss'],
})
export class FirmaprofilComponent implements OnInit {
  isUpdating = false;
  public aussteller$ = new Observable<Aussteller>();
  constructor(
    private _activatedRoute: ActivatedRoute,
    private _store: Store<{ ausstellers: AusstellersState }>,
    private _router: Router,
    @Inject(PLATFORM_ID) private platformId: Object
  ) {}

  ngOnInit(): void {
    const slug = this._activatedRoute.snapshot.params['slug'];
    this._store.dispatch(loadAussteller({ slug }));
    this.aussteller$ = this._store
      .select(selectAusstellerBySlug, { slug })
      .pipe(filter((aussteller) => !!aussteller),tap((a)=>console.log));
  }
  async aktualisieren(aussteller: Aussteller) {
    if (isPlatformBrowser(this.platformId)) {
      this.isUpdating = true;
      this._store.dispatch(updateAussteller({ aussteller }));
      this._store
        .pipe(
          select((state) => state.ausstellers.response),
          filter((l) => !!l)
        )
        .subscribe((d) => {
          const firma = d!.firma;
          this._store.dispatch(setResponseToNull());
          this.isUpdating = false;
          if (firma != aussteller.firma) {
            const route = firma!
              .toLocaleLowerCase()
              .replace(/\s+/g, '-')
              .replace(/[üöä]/g, (m: string) =>
                m === 'ü' ? 'ue' : m === 'ö' ? 'oe' : 'ae'
              );
            window.location.replace(
              `${window.location.origin}/Messestand/${route}`
            );
          } else window.location.reload();
        });
    }
  }
}
