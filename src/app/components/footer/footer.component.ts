import { Component, OnInit } from '@angular/core';
import {  MatDialog} from '@angular/material/dialog';
import { SocialMediaDialogComponent } from '../dialogs/social-media-dialog/social-media-dialog.component';

@Component({
  selector: 'skb-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  constructor(public dialog:MatDialog) { }


  ngOnInit(): void {
  }
  openDialog(media:string): void {
    const dialogRef = this.dialog.open(SocialMediaDialogComponent, {
      data:media,
      width: '600px'
    })
  }
}
