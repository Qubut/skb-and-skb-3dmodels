export interface PaginatorState {
  currentPage: number;
  pageSize: number;
  totalLength: number;
  pageSizeOptions:number[]
}
