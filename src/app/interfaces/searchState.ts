export interface SearchState {
  query: string;
  limit: number;
  offset: number;
  filter: string;
  indexName: string
  results: Stellenausschreibung[]|Aussteller[];
  loading: boolean;
  error: string;
  total:number
}

