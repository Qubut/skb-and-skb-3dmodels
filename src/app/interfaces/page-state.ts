import { ScrollPositionState } from "@skb/interfaces/scroll-position-state";

export interface pageState {
  scrollPosition: ScrollPositionState;
}
