export interface Homepage {
  id:number;
  begruessung: string;
  ueberschrift: string;
  zeitraum: string;
  messepartnertext: string;
  skblogo: Bild;
  begruessungsmedia: Begruessungsmedia[];
  alumniStorie: Alumnistory[];
  infokarten: Infokarte[];
  aussteller: { slug: string };
}
