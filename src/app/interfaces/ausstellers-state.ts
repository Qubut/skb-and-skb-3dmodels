export interface AusstellersState {
  response: Partial<Aussteller>|null
  entities: { [slug: string]: Aussteller };
  loaded: boolean;
  loading: boolean;
  error: any;
}
