import { Injectable } from '@angular/core';
import { MatPaginatorIntl } from '@angular/material/paginator';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class MatPaginatorTranslator extends MatPaginatorIntl {
  override itemsPerPageLabel = 'Einträge pro Seite';
  override nextPageLabel = 'Nächste Seite';
  override previousPageLabel = 'Vorherige Seite';
  override firstPageLabel = 'Erste Seite';
  override lastPageLabel = 'Letzte Seite';

  constructor(private translateService: TranslateService) {
    super();
  }

  override getRangeLabel = (page: number, pageSize: number, length: number) => {
    this.getTranslations();
    return (
      page * pageSize +
      1 +
      ' - ' +
      (page * pageSize + pageSize) +
      ' ' +
      this.translateService.instant('of') +
      ' ' +
      length
    );
  };

  private getTranslations() {
    this.itemsPerPageLabel = this.translateService.instant('Einträge pro Seite');
    this.nextPageLabel = this.translateService.instant('Nächste Seite');
    this.previousPageLabel = this.translateService.instant('Vorherige Seite');
    this.firstPageLabel = this.translateService.instant('Erste Seite');
    this.lastPageLabel = this.translateService.instant('Letzte Seite');
  }
}
