export interface HomepageState {
  homepage: Homepage|null;
  loading: boolean;
  error: any;
}
