export interface ScrollPositionState {
  [key: string]: number | undefined;
}
