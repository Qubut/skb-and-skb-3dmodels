import { StringifyOptions } from 'querystring';
import { StringLiteralLike } from 'typescript';
declare global {
  type document = {
    [field: string]: any;
  };
  type _matchesInfo<T> = Partial<
    Record<keyof T, Array<{ start: number; length: number }>>
  >;

  type Hit<T = document> = T & {
    _formatted?: Partial<T>;
    _matchesInfo?: _matchesInfo<T>;
  };

  type Hits<T = document> = Array<Hit<T>>;
  type Skb = {
    ausstellers: { data: [{ id: number; attributes: Aussteller }] };
  };

  type Aussteller = {
    id: number;
    ausstellerid: string;
    logo: Bild;
    firmenlogo: Bild;
    homepage: string;
    firma: string;
    strasse: string;
    adresszusatz: string;
    email: string;
    telefon: string;
    messetage: string;
    plz: string;
    land: string;
    ort: string;
    onlinemedien: OnlineMedien;
    printmedian: PrintMedien[];
    stellenausschreibungen: Stellenausschreibung[];
    aktualisieren: boolean;
    ansprechpartner: Ansprechpartner;
    messestand: Messestand;
    studiengangs: Studiengang[];
    slug: string;
  };
  type Messestand = { messestand: string };
  type OnlineMedien = {
    id?:number
    text: string;
    farbcode: string;
    unternehmensbeschreibung: string;
    messestand: string;
    videoUrls: string[];
    bilder: Bild[];
    text: string;
    flyer: Bild;
    unternehmensprofil: Bild;
    zoom: string[];
    socialmedien: string[];
    socialmedia?: {
      facebook: string;
      instagram: string;
      linkedin: string;
      xing: string;
      youtube: string;
      twitter: string;
    };
  };

  type PrintMedien = {
    id: number;
    branche: string;
    interesseSponsoring: string;
    text: string;
    angebot: string;
    sozialeEinrichtung: boolean;
    hauptsitz: string;
    anzahlMitarbeiter: string;
    messebuch: string;
    fakultaet: string[];
    logo: Bild;
    anzeige: Bild;
    standorte: string[];
  };

  type Stellenausschreibung = {
    id:number;
    aussteller: Aussteller;
    firma?: string;
    logo?: Bild;
    stellenausschreibungenId: number;
    datum: Date;
    art: string;
    studiengangs: Studiengang[];
    titel: string;
    file: Bild;
    studiengänge: string[];
  };

  type Schlagwort = string[];
  type Ansprechpartner = {
    id?: number;
    telefon: string;
    titel: string;
    vorname: string;
    name: string;
    email: string;
    anrede: string;
    bild: Bild;
  };

  type Bild = {
    id?:number;
    url: string;
    name?: string;
    height?: number;
    width?: number;
  };

  type Studiengang = { studiengang: string; checked?: boolen };

  type Fakultaet = {
    fakultaet: string;
    studiengangs: Studiengang[];
  };

  type Messepartners = {
    ausstellers: Aussteller | Aussteller[];
    premium_ausstellers: Aussteller | Aussteller[];
    premium_aussteller: Aussteller | Aussteller[];
  };

  type Gewinnspielsmessepartners = {
    gewinnspielsmessepartner: {
      data: {
        ausstellers: Aussteller[];
      };
    };
  };
  type Navigationsleiste = {
    data: { items: Item[] };
  };

  type Fachvortraege = {
    data: Fachvortrag[];
  };
  type Fachvortrag = {
    aussteller: AusstellerRest;
    title: string;
    text: string;
    datum: string;
    start_time: string;
    end_time: string;
  };
  type Item = {
    id: number;
    order: number;
    title: string;
    url: string;
    target: '_self' | '_blank' | '_parent';
    children: Item[];
    parent: Item | null;
  };
  type Jobart = {
    art: string;
    checked?: boolean;
  };
  type Homepage = {
    id:number
    begruessung:string;
    ueberschrift: string;
    zeitraum: string;
    begruessung: string;
    messepartnertext: string;
    skblogo: Bild;
    begruessungsmedia: Begruessungsmedia[];
    alumniStorie: Alumnistory[];
    infokarten: Infokarte[];
    aussteller: { slug: string };
  };
  type Begruessungsmedia = {
    ueberschrift: string;
    file: Bild;
    video: string;
    bild: Bild;
  };
  type Alumnistory = {
    name: string;
    beschreibung: string;
    bild: Bild[];
    storypdf: Bild;
  };
  type Infokarte = {
    beschreibung: text;
    titel: text;
    bild: Bild;
    media: {
      url: text;
    };
  };
  type PremiumAussteller = {
    firma: string;
    homepage: string;
    logo: Bild;
  };
}
