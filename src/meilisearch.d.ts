declare global{
  type document = {
  [field: string]: any
}
type _matchesInfo<T> = Partial<
  Record<keyof T, Array<{ start: number; length: number }>>
>

type Hit<T = document> = T & {
  _formatted?: Partial<T>
  _matchesInfo?: _matchesInfo<T>
}

type Hits<T = document> = Array<Hit<T>>
}
