import { BufferGeometry, Euler, Material, Mesh, Vector3 } from 'three';
import { Observable } from 'rxjs';
import { Texture } from 'three';
declare global{
  type Nodes = {
  [name: string]: Mesh<BufferGeometry, Material | Material[]>;
};


type Textures = Observable<Texture[] | Texture>;
type Urls = string | string[];

interface BasicData {
  rotation: Euler;
  scale: Vector3;
  position?: Vector3;
}
type BasicDataCollection = {
  a: BasicData;
  b: BasicData;
};
type Physical = {
  rotation: Euler;
  position?:Vector3
  scale?:Vector3
  material: {
    side:number;
    clearcoat: number;
    clearcoatRoughness: number;
    metalness: number;
    roughness: number;
  };
};
type LogosContent = {
  upperRightLogo: Physical;
  upperLeftLogo: Physical;
  lowerRightLogo: Physical
  lowerLeftLogo:Physical
  };
type Logos = {
  b: LogosContent;
  a: LogosContent;
};

}
