FROM node:16.14-alpine

ARG WORK_DIR=/build

ENV PATH=$(WORK_DIR)/node_modules/.bin:$PATH

RUN mkdir ${WORK_DIR}

WORKDIR ${WORK_DIR}
COPY package.json .
RUN npm -g i gzipper
RUN npm i -f
COPY . .

RUN npm run build:ssr

CMD ["node","dist/SKB-models/server/main.js"]
